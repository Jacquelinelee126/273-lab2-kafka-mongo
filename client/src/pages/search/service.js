import request from '../../utils/request';
import qs from 'qs';

export const queryJobs = (params) => {
    return request.get(`/search/jobs?${qs.stringify(params)}`);
};

export const queryAllEvents = (params) => {
    //console.log('===========> params: ', params, ' string:"', qs.stringify(params), '"');
    return request.get(`/student/event?${qs.stringify(params)}`);
};

export const queryAllStudents = (params) => {
    return request.get(`/student?${qs.stringify(params)}`);
};

export const queryAllJobs = (params) => {
    return request.get(`/student/job?${qs.stringify(params)}`);
};

export const queryEvents = (params) => {
    return request.get(`/search/events?${qs.stringify(params)}`);
};

export const queryOneStudent = (sID) => {
    //console.log("sID", sID);
    return request.get(`/student/profile/${sID}`);
};

export const queryOneJob = (jID) => {
    //console.log("sID", sID);
    return request.get(`/student/jobDetails/${jID}`);
};

export const queryOneCompany = (company) => {
    //console.log("sID", sID);
    return request.get(`/student/company/${company}`);
};

export const queryStudents = (params) => {
    return request.get(`/search/students?${qs.stringify(params)}`);
};

export const applyJob = (jID, data) => {
    return request.post(`/student/job/${jID}/apply`, data);
};

export const registerEvent = (eID) => {
    return request.post(`/student/event/${eID}/register`);
};
