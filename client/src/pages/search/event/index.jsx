import React, { useState, useEffect } from 'react';
import { Table, Popconfirm, message, Button, Pagination } from 'antd';
// import { useSelector } from "react-redux";

import SearchForm from './SearchForm';
import BasicLayout from '../../../layouts/BasicLayout';

import * as api from '../service';

export default function SearchPage(props) {
    const [{ total, events }, setState] = useState({ total: 0, events: [] });

    const onChange = async (page, limit) => {
        const { total, events } = await api.queryAllEvents({ page, limit });
        console.log(total, events);
        setState({ total, events });
    };
    useEffect(() => {onChange(1, 2)}, []);

    const handleSubmit = async (values) => {
        try {
            const { total, events } = await api.queryEvents(values);
            setState({ total, events });
        } catch (e) {
            console.log('e:');
        }
    };

    const handleApply = async (record) => {
        try {
            const res = await api.registerEvent(record._id);
            message.success('Registered Successfully!');
            console.log('res', res);
        } catch (e) {
            message.error(e);
            console.log('e:', e);
        }
    };

    const columns = [
        {
            title: 'Title',
            dataIndex: 'title',
            key: 'title',
        },
        {
            title: 'Location',
            dataIndex: 'location',
            key: 'location',
        },
        {
            title: 'Time',
            dataIndex: 'time',
            key: 'time',
        },
        {
            title: 'Description',
            dataIndex: 'description',
            key: 'description',
        },
        {
            title: 'Major',
            dataIndex: 'major',
            key: 'major',
        },
        // {
        //     title: 'Application Deadline',
        //     dataIndex: 'applicationDeadline',
        //     key: 'application_deadline',
        // },
        {
            title: 'Action',
            key: 'action',
            render: (record) => (
                <Popconfirm
                    title="Want to register?"
                    onConfirm={() => handleApply(record)}
                    okText="Yes"
                    cancelText="No"
                >
                    <Button>Register</Button>
                </Popconfirm>
            ),
        },
    ];

    return (
        <BasicLayout>
            <SearchForm onSubmit={handleSubmit} />
            <Table rowKey="_id" columns={columns} dataSource={events} pagination={false} />
            <Pagination
                size="large"
                total={total}
                showTotal={(total) => `Total ${total} events`}
                defaultPageSize={2}
                showQuickJumper={true}
                showSizeChanger={true}
                onChange={onChange}
                onShowSizeChange={onChange}
                pageSizeOptions={[1, 2, 5, 10, 20]}
            />
        </BasicLayout>
    );
}
