import React from 'react';
import { Form, Row, Col, Input, Button, Select } from 'antd';

const AdvancedSearchForm = (props) => {
    const [form] = Form.useForm();

    const onFinish = (values) => {
        props.onSubmit && props.onSubmit(values);
    };

    return (
        <Form
            form={form}
            name="advanced_search"
            className="ant-advanced-search-form"
            onFinish={onFinish}
        >
            <Row gutter={24}>
                <Col span={12}>
                    <Form.Item
                        name="keyword"
                        label="Keyword"
                        rules={[
                            {
                                required: true,
                                message: 'Input something!',
                            },
                        ]}
                    >
                        <Input placeholder="Search with event name" />
                    </Form.Item>
                </Col>
            </Row>
            <Row>
                <Col span={24} style={{ textAlign: 'right' }}>
                    <Button type="primary" htmlType="submit">
                        Search
                    </Button>
                    <Button
                        style={{ marginLeft: 8 }}
                        onClick={() => {
                            form.resetFields();
                        }}
                    >
                        Clear
                    </Button>
                </Col>
            </Row>
        </Form>
    );
};

export default AdvancedSearchForm;
