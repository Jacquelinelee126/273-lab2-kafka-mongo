import React from "react";
import { Table } from "antd";

export default function(props) {
  // const user = useSelector(state => state.user);
  const handleRegsiter = async (record) => {
    try {
        await api.registerEvent(record._id);
        message.success('Registered Successfully!');
    } catch (e) {
        message.error(e);
        console.error('e:', e);
    }
};
  const columns = [
    {
      title: "Title",
      dataIndex: "title",
      key: "title"
    },
    {
      title: "Location",
      dataIndex: "location",
      key: "location"
    },
    {
      title: "Description",
      dataIndex: "description",
      key: "description"
    },
    {
      title: "Time",
      dataIndex: "time",
      key: "time"
    },
    {
      title: "Major",
      dataIndex: "major",
      key: "major"
    },
    {
      title: 'Action',
      key: 'action',
      render: (record) => (
          <Popconfirm
              title="Want to register?"
              onConfirm={() => handleRegsiter(record)}
              okText="Yes"
              cancelText="No"
          >
              <Button>Register</Button>
          </Popconfirm>
      ),
  }
  ];

  return (
    <Table
     rowKey="_id"
      dataSource={props.list}
      columns={columns}
      pagination={true}
    />
  );
};