import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Table, Form, Button, Upload, message, Pagination } from 'antd';
import { UploadOutlined } from '@ant-design/icons';

// import { useSelector } from "react-redux";

import SearchForm from './SearchForm';
import BasicLayout from '../../../layouts/BasicLayout';

import * as api from '../service';

export default function SearchPage() {
    const [{ jobs, total }, setState] = useState({ jobs: [], total: 0 });
    // const user = useSelector(state => state.user);
    const onChange = async (page, limit) => {
        const { total, jobs } = await api.queryAllJobs({ page, limit });
        setState({ total, jobs });
    };
    // use [] will only call useEffect once
    useEffect(() => {
        onChange(1, 2);
    }, []);
    const handleSubmit = async (values) => {
        try {
            const { total, jobs } = await api.queryJobs(values);
            setState({ total, jobs });
        } catch (e) {
            console.log('e:', e);
        }
    };

    const normFile = (e) => {
        console.log('Upload event:', e);
        if (Array.isArray(e)) {
            return e;
        }
        return e && e.fileList;
    };

    const columns = [
        {
            title: 'Title',
            dataIndex: 'title',
            key: 'title',
            render: (text, record) => <Link to={`/student/job/${record._id}`}>{text}</Link>,
        },
        {
            title: 'Location',
            dataIndex: 'location',
            key: 'location',
        },
        {
            title: 'Salary',
            dataIndex: 'salary',
            key: 'salary',
        },
        {
            title: 'Company',
            dataIndex: 'company',
            key: 'company',
            render: (company) =>
                company ? (
                    <Link to={`/student/company/${company.name}`}>{company.name} </Link>
                ) : (
                    'N/A'
                ),
        },
        {
            title: 'Application Deadline',
            dataIndex: 'applicationDeadline',
            key: 'application_deadline',
        },
        {
            title: 'Action',
            key: 'action',
            //             var formdata = new FormData();
            // formdata.append("resume", fileInput.files[0], "Screen Shot 2020-03-13 at 12.52.04.png");
            render: (record) => (
                <Form.Item
                    name="upload"
                    label="Upload"
                    valuePropName="fileList"
                    getValueFromEvent={normFile}
                >
                    <Upload
                        name="resume"
                        customRequest={async ({ file }) => {
                            const formdata = new FormData();
                            formdata.append('resume', file);
                            try {
                                await api.applyJob(record._id, formdata);
                                message.success('Apllied Successfully!');
                            } catch (e) {
                                message.error('You have applied already');
                            }
                        }}
                        listType="picture"
                    >
                        <Button>
                            <UploadOutlined /> Upload resume to apply
                        </Button>
                    </Upload>
                </Form.Item>
            ),
        },
    ];

    return (
        <BasicLayout>
            <SearchForm onSubmit={handleSubmit} />

            <Table rowKey="_id" columns={columns} dataSource={jobs} pagination={false} />
            <Pagination
                size="large"
                total={total}
                showTotal={(total) => `Total ${total} jobs`}
                defaultPageSize={2}
                showQuickJumper={true}
                showSizeChanger={true}
                pageSizeOptions={[1, 2, 5, 10, 20]}
                onChange={onChange}
                onShowSizeChange={onChange}
            />
        </BasicLayout>
    );
}
