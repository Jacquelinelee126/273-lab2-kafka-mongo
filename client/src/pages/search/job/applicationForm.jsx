import React from 'react';
import { Form, Row, Col, Input, Button, Select } from 'antd';

const AdvancedSearchForm = (props) => {
    const [form] = Form.useForm();

    const onFinish = (values) => {
        props.onSubmit && props.onSubmit(values);
    };

    return (
        <Form
            form={form}
            name="advanced_search"
            className="ant-advanced-search-form"
            onFinish={onFinish}
        >
            <Row gutter={24}>
                <Col span={8}>
                    <Form.Item
                        name="keyword"
                        label="Keyword"
                        rules={[
                            {
                                required: true,
                                message: 'Input something!',
                            },
                        ]}
                    >
                        <Input placeholder="Search with title or company_name" />
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="category" label="Category">
                        <Select>
                            <Select.Option value="full_time">Full Time</Select.Option>
                            <Select.Option value="part_time">Part Time</Select.Option>
                            <Select.Option value="intern">Intern</Select.Option>
                            <Select.Option value="on_campus">On Campus</Select.Option>
                        </Select>
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="location" label="Location">
                        <Input placeholder="Job Location" />
                    </Form.Item>
                </Col>
            </Row>
            <Row>
                <Col span={24} style={{ textAlign: 'right' }}>
                    <Button type="primary" htmlType="submit">
                        Search
                    </Button>
                    <Button
                        style={{ marginLeft: 8 }}
                        onClick={() => {
                            form.resetFields();
                        }}
                    >
                        Clear
                    </Button>
                </Col>
            </Row>
        </Form>
    );
};

export default AdvancedSearchForm;
