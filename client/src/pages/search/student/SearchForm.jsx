import React from 'react';
import { Form, Row, Col, Input, Button, Select } from 'antd';

const AdvancedSearchForm = (props) => {
    const [form] = Form.useForm();

    const onFinish = (values) => {
        props.onSubmit && props.onSubmit(values);
    };

    return (
        <Form
            form={form}
            name="search-student"
            className="ant-advanced-search-form"
            onFinish={onFinish}
        >
            <Row gutter={24}>
                <Col span={8}>
                    <Form.Item
                        name="keyword"
                        label="Keyword"
                        rules={[
                            {
                                required: true,
                                message: 'Input something!',
                            },
                        ]}
                    >
                        <Input placeholder="Search students using name" />
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item name="major" label="Major">
                    <Select>
                        <Select.Option value="Software Engineering">Software Engineering</Select.Option>
                        <Select.Option value="Electrical Engineering">Electrical Engineering</Select.Option>
                        <Select.Option value="ommunications">Communications</Select.Option>
                        <Select.Option value="Business">Business</Select.Option>
                        <Select.Option value="Economics">Economics</Select.Option>
                        <Select.Option value="Literature">Literature</Select.Option>
                        <Select.Option value="Psychology">Psychology</Select.Option>
                        <Select.Option value="Nursing">Nursing</Select.Option>
                        <Select.Option value="Computer Science">Computer Science</Select.Option>
                    </Select>
                    </Form.Item>
                </Col>
            </Row>
            <Row>
                <Col span={24} style={{ textAlign: 'right' }}>
                    <Button type="primary" htmlType="submit">
                        Search
                    </Button>
                    <Button
                        style={{ marginLeft: 8 }}
                        onClick={() => {
                            form.resetFields();
                        }}
                    >
                        Clear
                    </Button>
                </Col>
            </Row>
        </Form>
    );
};

export default AdvancedSearchForm;
