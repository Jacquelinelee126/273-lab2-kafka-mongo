import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Table, Pagination, message } from 'antd';
// import { useSelector } from "react-redux";

import SearchForm from './SearchForm';
import BasicLayout from '../../../layouts/BasicLayout';

import * as api from '../service';

export default function SearchPage(props) {
    const [{ students, total }, setState] = useState({ students: [], total: 0 });
    console.log(useState());

    const onChange = async (page, limit) => {
        try {
            const { total, students } = await api.queryAllStudents({ page, limit });
            setState({ total, students });
        } catch (e) {
            message.error(e);
        }
       
    };

    useEffect(() => {onChange(1, 5)}, []);

    const handleSubmit = async (values) => {
        try {
            const {students} = await api.queryStudents(values);
            setState({total, students});
        } catch (e) {
            console.log('e:', e);
        }
    };

    const columns = [
        {
            title: 'UserName',
            dataIndex: 'username',
            key: 'title',
            render: (text, record) => <Link to={`/student/profile/${record._id}`}>{text}</Link>,
            //render: (text, record) => <div><Link to="/">{text}</Link></div>,

        },
        {
            title: 'Name',
            dataIndex: 'basic',
            key: 'title',
            render: (basic) => (basic[0] ? basic[0].name : 'N/A'),
        },
        {
            title: 'Major',
            dataIndex: 'education',
            key: 'location',
            render: (education) => (education[0] ? education[0].major : 'N/A'),
        },
        {
            title: 'College',
            dataIndex: 'education',
            key: 'collegeName',
            render: (education) => (education[0] ? education[0].collegeName : 'N/A'),
        },
    ];

    return (
        <BasicLayout>
            <SearchForm onSubmit={handleSubmit} />
            <Table rowKey="_id" columns={columns} dataSource={students} pagination={false}/>
            <Pagination
                size="large"
                total={total}
                showTotal={(total) => `Total ${total} students`}
                defaultPageSize={5}
                showQuickJumper={true}
                showSizeChanger={true}
                onChange={onChange}
                pageSizeOptions={[1, 2, 5, 10, 20]}
                onShowSizeChange={onChange}
            />
        </BasicLayout>
    );
}
