import { createSlice } from "@reduxjs/toolkit";
import { push } from "connected-react-router";

import * as api from "./service";

const initialState = {
  loading: false,
  error: null
};

const basicSlice = createSlice({
  name: "basic",
  initialState,
  reducers: {
    asyncStart: state => ({
      ...state,
      loading: true,
      error: null
    }),
    asyncFinish: state => ({
      ...state,
      loading: false,
      error: null
    }),
    asyncError: state => ({
      ...state,
      loading: false,
      error: null
    }),
    updateBasic: (state, action) => ({
      ...state,
      ...action.payload
    })
  }
});

const { actions, reducer } = basicSlice;

const { asyncStart, asyncFinish, asyncError, updateBasic } = actions;

const effects = {
  queryBasic: () => async dispatch => {
    dispatch(asyncStart());
    try {
      const result = await api.queryBasic();
      dispatch(updateBasic(result));
      dispatch(asyncFinish());
    } catch (e) {
      dispatch(asyncError(e));
      throw e;
    }
  },
  createBasic: (data) => async dispatch => {
    dispatch(asyncStart());
    try {
      const result = await api.createBasic(data);
      dispatch(updateBasic(result));
      dispatch(asyncFinish());
      dispatch(push("/student/profile"));
    } catch (e) {
      dispatch(asyncError(e));
      throw e;
    }
  },
  updateBasic: (data) => async dispatch => {
    dispatch(asyncStart());
    try {
      const result = await api.updateBasic(data);
      console.log('result:', result);
      dispatch(updateBasic(result));
      dispatch(asyncFinish());
      dispatch(push("/student/profile"));
    } catch (e) {
      dispatch(asyncError(e));
      throw e;
    }
  }
};

export { actions, reducer, effects };
