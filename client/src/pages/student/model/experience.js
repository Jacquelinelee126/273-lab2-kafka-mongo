import { createSlice } from '@reduxjs/toolkit';
import { push } from 'connected-react-router';

import * as api from './service';

const initialState = {
    loading: false,
    error: null,
    items: [],
};

const experienceSlice = createSlice({
    name: 'experience',
    initialState,
    reducers: {
        asyncStart: (state) => ({
            ...state,
            loading: true,
            error: null,
        }),
        asyncFinish: (state) => ({
            ...state,
            loading: false,
            error: null,
        }),
        asyncError: (state) => ({
            ...state,
            loading: false,
            error: null,
        }),
        updateExperience: (state, action) => ({
            ...state,
            ...action.payload,
        }),
        updateOne: (state, action) => {
            const index = state.items.findIndex((el) => el._id === action.payload._id);
            state.items.splice(index, 1, action.payload);
        },
        updateItems: (state, action) => ({
            ...state,
            items: [...action.payload],
        }),
    },
});

const { actions, reducer } = experienceSlice;

const { asyncStart, asyncFinish, asyncError, updateItems, updateOne } = actions;

const effects = {
    queryExperienceById: (id) => async (dispatch) => {
        dispatch(asyncStart());
        try {
            const result = await api.queryExperienceById(id);
            dispatch(updateOne(result));
            dispatch(asyncFinish());
            return result;
        } catch (e) {
            dispatch(asyncError(e));
            throw e;
        }
    },
    queryExperiences: () => async (dispatch) => {
        dispatch(asyncStart());
        try {
            const result = await api.queryExperiences();
            dispatch(updateItems(result));
            dispatch(asyncFinish());
        } catch (e) {
            dispatch(asyncError(e));
            throw e;
        }
    },
    createExperience: (data) => async (dispatch) => {
        dispatch(asyncStart());
        try {
            const result = await api.createExperience(data);
            dispatch(updateOne(result));
            dispatch(asyncFinish());
            dispatch(push('/student/profile'));
        } catch (e) {
            dispatch(asyncError(e));
            throw e;
        }
    },
    updateExperience: (id, data) => async (dispatch) => {
        dispatch(asyncStart());
        try {
            const result = await api.updateExperience(id, data);
            console.log('result:', result);
            dispatch(updateOne(result));
            dispatch(asyncFinish());
            dispatch(push('/student/profile'));
        } catch (e) {
            dispatch(asyncError(e));
            throw e;
        }
    },
};

export { actions, reducer, effects };
