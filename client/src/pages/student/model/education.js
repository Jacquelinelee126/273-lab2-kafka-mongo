import { createSlice } from "@reduxjs/toolkit";
import { push } from "connected-react-router";

import * as api from "./service";

const initialState = {
  loading: false,
  error: null
};

const educationSlice = createSlice({
  name: "education",
  initialState,
  reducers: {
    asyncStart: state => ({
      ...state,
      loading: true,
      error: null
    }),
    asyncFinish: state => ({
      ...state,
      loading: false,
      error: null
    }),
    asyncError: state => ({
      ...state,
      loading: false,
      error: null
    }),
    updateEducation: (state, action) => ({
      ...state,
      ...action.payload
    })
  }
});

const { actions, reducer } = educationSlice;

const { asyncStart, asyncFinish, asyncError, updateEducation } = actions;

const effects = {
  queryEducation: () => async dispatch => {
    dispatch(asyncStart());
    try {
      const result = await api.queryEducation();
      dispatch(updateEducation(result));
      dispatch(asyncFinish());
    } catch (e) {
      dispatch(asyncError(e));
      throw e;
    }
  },
  createEducation: (data) => async dispatch => {
    dispatch(asyncStart());
    try {
      const result = await api.createEducation(data);
      dispatch(updateEducation(result));
      dispatch(asyncFinish());
      dispatch(push("/student/profile"));
    } catch (e) {
      dispatch(asyncError(e));
      throw e;
    }
  },
  updateEducation: (data) => async dispatch => {
    dispatch(asyncStart());
    try {
      const result = await api.updateEducation(data);
      console.log('result:', result);
      dispatch(updateEducation(result));
      dispatch(asyncFinish());
      dispatch(push("/student/profile"));
    } catch (e) {
      dispatch(asyncError(e));
      throw e;
    }
  }
};

export { actions, reducer, effects };
