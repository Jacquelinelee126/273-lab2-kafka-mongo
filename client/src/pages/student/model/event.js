import { createSlice } from '@reduxjs/toolkit';
import { push } from 'connected-react-router';

import * as api from './service';

const initialState = {
    loading: false,
    error: null,
    list: [],
    registration: [],
};

const eventSlice = createSlice({
    name: 'event',
    initialState,
    reducers: {
        asyncStart: (state) => ({
            ...state,
            loading: true,
            error: null,
        }),
        asyncFinish: (state) => ({
            ...state,
            loading: false,
            error: null,
        }),
        asyncError: (state) => ({
            ...state,
            loading: false,
            error: null,
        }),
        updateOne: (state, action) => {
            const index = state.list.findIndex((el) => el._id === action.payload._id);
            state.list.splice(index, 1, action.payload);
        },
        updateList: (state, action) => ({
            ...state,
            list: [...action.payload],
        }),
        updateRegistration: (state, action) => ({
            ...state,
            registration: action.payload,
        }),
        updateRegistrationOne: (state, action) => {
            const index = state.registration.findIndex((el) => el._id === action.payload._id);
            state.registration.splice(index, 1, action.payload);
        },
    },
});

const { actions, reducer } = eventSlice;

const {
    asyncStart,
    asyncFinish,
    asyncError,
    updateList,
    updateOne,
    updateRegistration,
} = actions;

const effects = {
    queryEvents: () => async (dispatch) => {
        dispatch(asyncStart());
        try {
            const result = await api.queryEvents();
            dispatch(updateList(result));
            dispatch(asyncFinish());
        } catch (e) {
            dispatch(asyncError(e));
            throw e;
        }
    },
    queryEventById: (id) => async (dispatch) => {
        dispatch(asyncStart());
        try {
            const result = await api.queryEventById(id);
            dispatch(updateOne(result));
            dispatch(asyncFinish());
            return result;
        } catch (e) {
            dispatch(asyncError(e));
            throw e;
        }
    },
    createEvent: (data) => async (dispatch) => {
        dispatch(asyncStart());
        try {
            const result = await api.createEvent(data);
            dispatch(updateOne(result));
            dispatch(asyncFinish());

            dispatch(push('/company/Events'));
        } catch (e) {
            dispatch(asyncError(e));
            throw e;
        }
    },
    updateEvent: (eID, data) => async (dispatch) => {
        dispatch(asyncStart());
        try {
            const result = await api.updateEvent(eID, data);
            dispatch(updateOne(result));
            dispatch(asyncFinish());
            dispatch(push('/company/Events'));
        } catch (e) {
            dispatch(asyncError(e));
            throw e;
        }
    },
    queryRegistration: (eID) => async (dispatch) => {
        dispatch(asyncStart());
        try {
            const result = await api.queryEventRegistrations(eID);
            dispatch(updateRegistration(result));
            dispatch(asyncFinish());
        } catch (e) {
            dispatch(asyncError(e));
            throw e;
        }
    },
};

export { actions, reducer, effects };
