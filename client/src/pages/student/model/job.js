// import { createSlice } from '@reduxjs/toolkit';
// import { push } from 'connected-react-router';

// import * as api from './service';

// const initialState = {
//     loading: false,
//     error: null,
//     list: [],
//     application: [],
// };

// const jobSlice = createSlice({
//     name: 'job',
//     initialState,
//     reducers: {
//         asyncStart: (state) => ({
//             ...state,
//             loading: true,
//             error: null,
//         }),
//         asyncFinish: (state) => ({
//             ...state,
//             loading: false,
//             error: null,
//         }),
//         asyncError: (state) => ({
//             ...state,
//             loading: false,
//             error: null,
//         }),
//         updateOne: (state, action) => {
//             const index = state.list.findIndex((el) => el._id === action.payload._id);
//             state.list.splice(index, 1, action.payload);
//         },
//         updateList: (state, action) => ({
//             ...state,
//             list: [...action.payload],
//         }),
//         updateApplication: (state, action) => ({
//             ...state,
//             application: action.payload,
//         }),
//         updateApplicationOne: (state, action) => {
//             const index = state.application.findIndex((el) => el._id === action.payload._id);
//             state.application.splice(index, 1, action.payload);
//         },
//     },
// });

// const { actions, reducer } = jobSlice;

// const {
//     asyncStart,
//     asyncFinish,
//     asyncError,
//     updateList,
//     updateOne,
//     updateApplication,
//     updateApplicationOne,
// } = actions;

// const effects = {
//     queryJobs: () => async (dispatch) => {
//         dispatch(asyncStart());
//         try {
//             const result = await api.queryJobs();
//             dispatch(updateList(result));
//             dispatch(asyncFinish());
//         } catch (e) {
//             dispatch(asyncError(e));
//             throw e;
//         }
//     },
//     queryJobById: (id) => async (dispatch) => {
//         dispatch(asyncStart());
//         try {
//             const result = await api.queryJobById(id);
//             dispatch(updateOne(result));
//             dispatch(asyncFinish());
//             return result;
//         } catch (e) {
//             dispatch(asyncError(e));
//             throw e;
//         }
//     },
//     createJob: (data) => async (dispatch) => {
//         dispatch(asyncStart());
//         try {
//             const result = await api.createJob(data);
//             dispatch(updateOne(result));
//             dispatch(asyncFinish());

//             dispatch(push('/company/job'));
//         } catch (e) {
//             dispatch(asyncError(e));
//             throw e;
//         }
//     },
//     updateJob: (jID, data) => async (dispatch) => {
//         dispatch(asyncStart());
//         try {
//             const result = await api.updateJob(jID, data);
//             dispatch(updateOne(result));
//             dispatch(asyncFinish());
//             dispatch(push('/company/job'));
//         } catch (e) {
//             dispatch(asyncError(e));
//             throw e;
//         }
//     },
//     queryApplication: (jID) => async (dispatch) => {
//         dispatch(asyncStart());
//         try {
//             const result = await api.queryJobApplications(jID);
//             dispatch(updateApplication(result));
//             dispatch(asyncFinish());
//         } catch (e) {
//             dispatch(asyncError(e));
//             throw e;
//         }
//     },
//     changeApplicationStatus: (aID, data) => async (dispatch) => {
//         dispatch(asyncStart());
//         try {
//             const result = await api.updateJobApplicationStatus(aID, data);
//             dispatch(updateApplicationOne(result));
//             dispatch(asyncFinish());
//         } catch (e) {
//             dispatch(asyncError(e));
//             throw e;
//         }
//     },
// };

// export { actions, reducer, effects };
