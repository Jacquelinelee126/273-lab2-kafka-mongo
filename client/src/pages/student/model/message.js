import { createSlice } from "@reduxjs/toolkit";
import { push } from "connected-react-router";

import * as api from "./service";

const initialState = {
  loading: false,
  error: null
};

const messageSlice = createSlice({
  name: "message",
  initialState,
  reducers: {
    asyncStart: state => ({
      ...state,
      loading: true,
      error: null
    }),
    asyncFinish: state => ({
      ...state,
      loading: false,
      error: null
    }),
    asyncError: state => ({
      ...state,
      loading: false,
      error: null
    }),
    updateMessage: (state, action) => ({
      ...state,
      ...action.payload
    })
  }
});

const { actions, reducer } = messageSlice;

const { asyncStart, asyncFinish, asyncError, updateMessage } = actions;

const effects = {
  // queryMessage: () => async dispatch => {
  //   dispatch(asyncStart());
  //   try {
  //     const result = await api.queryMessage();
  //     dispatch(updateMessage(result));
  //     dispatch(asyncFinish());
  //   } catch (e) {
  //     dispatch(asyncError(e));
  //     throw e;
  //   }
  // },
  createMessage: (data) => async dispatch => {
    dispatch(asyncStart());
    try {
      const result = await api.createMessage(data);
      // dispatch(updateMessage(result));
      dispatch(asyncFinish());
      dispatch(push("/student/profile"));
    } catch (e) {
      dispatch(asyncError(e));
      throw e;
    }
  },
  // updateMessage: (data) => async dispatch => {
  //   dispatch(asyncStart());
  //   try {
  //     const result = await api.updateMessage(data);
  //     console.log('result:', result);
  //     dispatch(updateMessage(result));
  //     dispatch(asyncFinish());
  //     dispatch(push("/student/profile"));
  //   } catch (e) {
  //     dispatch(asyncError(e));
  //     throw e;
  //   }
  // }
};

export { actions, reducer, effects };
