import request from '../../../utils/request';
import qs from 'qs';
export const queryProfile = () => {
    return request.get(`/student`);
};

export const queryBasic = () => {
    return request.get(`/basic`);
};

export const createBasic = (data) => {
    return request.post(`/basic`, data);
};

export const updateBasic = (data) => {
    return request.put(`/basic`, data);
};

export const queryEducation = () => {
    return request.get(`/education`);
};

export const createEducation = (data) => {
    return request.post(`/education`, data);
};

export const updateEducation = (data) => {
    return request.put(`/education`, data);
};

export const queryExperiences = () => {
    return request.get(`/experience`);
};

export const queryExperienceById = (id) => {
    return request.get(`/experience/${id}`);
};

export const createExperience = (data) => {
    return request.post(`/experience`, data);
};

export const updateExperience = (id, data) => {
    return request.put(`/experience/${id}`, data);
};

export const queryApplications = (params) => {
    console.log(params);
    return request.get(`/student/job/applications?${qs.stringify(params)}`);
};

export const createMessage = (data) => {};
