/**
 * @file student user profile page
 * @description
 * Display if user has created a profile otherwise
 * allow user to add one by clicking "create" button
 */
import { Descriptions, message, Button } from 'antd';
import { Link, useParams } from 'react-router-dom';


import React, { useEffect } from 'react';
import { useState } from 'react';

import Basic from '../student/profile/basic/components/Basic';
import Education from '../student/profile/education/components/Education';
// import Experience from '../student/profile/experience/components/Experience';
// import Message from '../student/profile/message/component/Message';

import * as api from '../search/service';

const OneStudentProfile = () => {
    const { sID } = useParams();
    console.log('One SID:', sID);

    const [currentStudent, setCurrentStudent] = useState();
    // const [loading, setLoading] = useState(false); // loading

    // try catch
    useEffect(() => {
        async function listOneStudent() {
            try {
                const currentStudent = await api.queryOneStudent(sID);
                setCurrentStudent(currentStudent);
                // setLoading(true);
            } catch (e) {
                message.error(e);
                // setLoading(false);
            }
        }
        listOneStudent();
    }, []);

    // if (loading) {
    //     return <div>loading...</div>;
    // }
    // if (!loading) {
    //     return <div>Error...</div>;
    // }

    console.log('currentStudent', currentStudent);
    if (!currentStudent || Object.keys(currentStudent).length === 0) {
        return <h1>This student hasn't uploaded his profile yet!</h1>;
    }
    // reuse component and check user id

    const { basic, education, experience } = currentStudent;
    return (
        <div>
            <Button type="primary">
                <Link to="/student/profile/message">Message</Link>
            </Button>
            <Basic {...basic}></Basic>
            <Education {...education}></Education>
        </div>
        // <div>
        //     <Descriptions title="Basic">
        //         <Descriptions.Item label="Name">{basic.name}</Descriptions.Item>
        //         <Descriptions.Item label="Birth">{basic.dob}</Descriptions.Item>
        //             <Descriptions.Item label="City">{basic.city}</Descriptions.Item>
        //             <Descriptions.Item label="State">{basic.state}</Descriptions.Item>
        //             <Descriptions.Item label="Country">{basic.country}</Descriptions.Item>
        //             <Descriptions.Item label="Skillset">{basic.skillset}</Descriptions.Item>
        //             <Descriptions.Item label="Career Objective">
        //                 {basic.careerObjective}
        //             </Descriptions.Item>
        //     </Descriptions>

        //     <Descriptions title="Education">
        //         <Descriptions.Item label="Name">{education.name}</Descriptions.Item>
        //         <Descriptions.Item label="Birth">{education.dob}</Descriptions.Item>
        //             <Descriptions.Item label="City">{basic.city}</Descriptions.Item>
        //             <Descriptions.Item label="State">{basic.state}</Descriptions.Item>
        //             <Descriptions.Item label="Country">{basic.country}</Descriptions.Item>
        //             <Descriptions.Item label="Skillset">{basic.skillset}</Descriptions.Item>
        //             <Descriptions.Item label="Career Objective">
        //                 {basic.careerObjective}
        //             </Descriptions.Item>
        //     </Descriptions>

        //     <Descriptions title="Basic">
        //         <Descriptions.Item label="Name">{basic.name}</Descriptions.Item>
        //         <Descriptions.Item label="Birth">{basic.dob}</Descriptions.Item>
        //             <Descriptions.Item label="City">{basic.city}</Descriptions.Item>
        //             <Descriptions.Item label="State">{basic.state}</Descriptions.Item>
        //             <Descriptions.Item label="Country">{basic.country}</Descriptions.Item>
        //             <Descriptions.Item label="Skillset">{basic.skillset}</Descriptions.Item>
        //             <Descriptions.Item label="Career Objective">
        //                 {basic.careerObjective}
        //             </Descriptions.Item>
        //     </Descriptions>
        // </div>
    );
};

export default OneStudentProfile;

// if (student.loading) {
//     return <div>loading...</div>;
// }

// if (student.error) {
//     return <div>error</div>;
// }

// create if no
// if (!student.id) {
//     return (
//         <div className="profile">
//             <div className="message">
//                 <Button type="primary">
//                     <Link to="/student/profile/message">Message</Link>
//                 </Button>
//             </div>
//             <div className="basic">
//                 {/* <h3>It seems that your profile is empty. Create a profile to let us know more about you</h3> */}
//                 {basic._id ? (
//                     <div>
//                         {/* <Card title="Basic" bordered={true} style={{ width: 300 }}>
//                 <p>Card content</p>
//                 <p>Card content</p>
//                 <p>Card content</p>
//                 <ProfileBasic {...basic} />
//             </Card> */}
//                         <Basic {...basic} />
//                         <Button type="primary">
//                             <Link to="/student/profile/basic/edit">Edit</Link>
//                         </Button>
//                     </div>
//                 ) : (
//                     <Button type="primary">
//                         <Link to="/student/profile/basic/create">Create</Link>
//                     </Button>
//                 )}
//             </div>
//             <div className="education">
//                 {education._id ? (
//                     <div>
//                         {/* <Card title="Basic" bordered={true} style={{ width: 300 }}>
//                 <p>Card content</p>
//                 <p>Card content</p>
//                 <p>Card content</p>
//                 <ProfileBasic {...basic} />
//             </Card> */}
//                         <Education {...education} />
//                         <Button type="primary">
//                             <Link to="/student/profile/education/edit">Edit</Link>
//                         </Button>
//                     </div>
//                 ) : (
//                     <Button type="primary">
//                         <Link to="/student/profile/education/create">Add Education Experience</Link>
//                     </Button>
//                 )}
//             </div>

//             <div className="experience">
//                 <div>
//                     <Experience {...experience} />
//                 </div>
//                 <br></br>
//                 <br></br>
//                 <Button type="primary">
//                     <Link to="/student/profile/experience/create">Add Work Experience</Link>
//                 </Button>
//             </div>
//         </div>
//     );
// };
