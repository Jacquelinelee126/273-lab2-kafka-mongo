import React, { useState, useEffect } from 'react';
import { Table, Pagination } from 'antd';
import { useSelector } from 'react-redux';

import * as api from '../model/service';

export default function ApplicationsPage() {
    const [{ total, applications }, setState] = useState({total:0,applications:[]});
    const onChange = async (page, limit) => {
        const { total, applications } = await api.queryApplications({ page, limit });
        setState({ total, applications });
    };

    useEffect(() => {
        onChange(1, 2);
    }, []);

    const columns = [
        // {
        //     title: 'Job',
        //     dataIndex: '_id',
        //     key: 'job_id',
        // },
        {
            title: 'Job Title',
            dataIndex: 'job',
            key: 'title',
            render: (job) => (job ? job.title : 'N/A'),
        },
        {
            title: 'Applied Date',
            dataIndex: 'createdAt',
            key: 'appliedDate',
        },
        {
            title: 'Status',
            dataIndex: 'status',
            key: 'status',
        },
    ];

    return (
        <>
            <Table rowKey="id" columns={columns} dataSource={applications} pagination={false} />
            <Pagination
                size="large"
                total={total}
                showTotal={(total) => `Total ${total} applications`}
                defaultCurrent={1}
                defaultPageSize={2}
                showQuickJumper={true}
                showSizeChanger={true}
                pageSizeOptions={[1, 2, 5, 10, 20]}
                onChange={onChange}
                onShowSizeChange={onChange}
            />
        </>
    );
}
