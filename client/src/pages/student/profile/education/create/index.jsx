import React from 'react';
import { useDispatch } from 'react-redux';

import { effects } from '../../../model/education';
import EducationForm from '../components/EducationForm';

export default function EducationPage() {
    const dispatch = useDispatch();

    const handleCreate = (values) => {
        dispatch(
            effects.createEducation({
                ...values,
            }),
        );
    };

    return (
        <div>
            <EducationForm onSubmit={handleCreate} />
        </div>
    );
}
