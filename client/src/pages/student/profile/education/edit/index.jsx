import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { message } from 'antd';

import { effects } from '../../../model/education';
import EducationForm from '../components/EducationForm';

export default function () {
    const { education } = useSelector((state) => ({
        education: state.education,
    }));
    const dispatch = useDispatch();

    useEffect(() => {
        if (!education._id) {
            dispatch(effects.queryEducationById());
        }
    }, [dispatch, education._id]);

    const handleUpdate = async (values) => {
        try {
            await dispatch(
                effects.updateEducation({
                    ...values,
                }),
            );
            message.success('Successfully updated.');
        } catch (e) {}
    };

    if (education.loading) {
        return <div>loading...</div>;
    }

    if (education.error) {
        return <div>error</div>;
    }

    return (
        <div>
            <EducationForm initialValues={education} onSubmit={handleUpdate} />
        </div>
    );
}
