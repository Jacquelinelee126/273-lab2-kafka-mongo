import React from 'react';
import { useDispatch } from 'react-redux';

import { effects } from '../../../model/basic';
import BasicForm from '../components/BasicForm';

export default function BasicPage () {
    const dispatch = useDispatch();

    const handleCreate = (values) => {
        dispatch(
            effects.createBasic({
                ...values,
            }),
        );
    };

    return (
        <div>
            <BasicForm onSubmit={handleCreate} />
        </div>
    );
};

