import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { message } from 'antd';

import { effects } from '../../../model/basic';
import BasicForm from '../components/BasicForm';

export default function () {
    const { basic } = useSelector((state) => ({
        basic: state.basic,
    }));
    const dispatch = useDispatch();

    useEffect(() => {
        if (!basic._id) {
            dispatch(effects.queryBasic());
        }
    }, [dispatch, basic._id]);

    const handleUpdate = async (values) => {
        try {
            console.log("Inside edit basic")
            await dispatch(effects.updateBasic({...values}));
            message.success('Successfully updated.');
        } catch (e) {
            message.error('Update failed.')
        }
    };

    if (basic.loading) {
        return <div>loading...</div>;
    }

    if (basic.error) {
        return <div>error</div>;
    }

    return (
        <div>
            <BasicForm initialValues={basic} onSubmit={handleUpdate} />
        </div>
    );
}
