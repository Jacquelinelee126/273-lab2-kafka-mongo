import React from 'react';
import { Descriptions } from 'antd';
export default function (props) {
    return (
        <div>
            <Descriptions title="Basic">
                <Descriptions.Item label="Name">{props.name}</Descriptions.Item>
                <Descriptions.Item label="Birth">{props.dob}</Descriptions.Item>
                <Descriptions.Item label="City">{props.city}</Descriptions.Item>
                <Descriptions.Item label="State">{props.state}</Descriptions.Item>
                <Descriptions.Item label="Country">{props.country}</Descriptions.Item>
                <Descriptions.Item label="Skillset">{props.skillset}</Descriptions.Item>
                <Descriptions.Item label="Career Objective">
                    {props.careerObjective}
                </Descriptions.Item>
            </Descriptions>
        </div>
    );
}
