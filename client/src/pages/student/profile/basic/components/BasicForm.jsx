import React from 'react';
import { Form, Input, Button, DatePicker } from 'antd';
import moment from 'moment';

const formItemLayout = {
    labelCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 8,
        },
    },
    wrapperCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 16,
        },
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

const config = {
    rules: [{ type: 'object', required: true, message: 'Please select time!' }],
};

const BasicForm = ({ onSubmit, initialValues = {} }) => {
    const [form] = Form.useForm();

    // console.log('form', form);

    const onFinish = (values) => {
        const { dob, ...others } = values;

        onSubmit &&
            onSubmit({
                ...others,
                dob: moment(dob).format('YYYY-MM-DD'),
            });
    };

    return (
        <div className="basic-form">
            <Form
                {...formItemLayout}
                form={form}
                name="basic"
                onFinish={onFinish}
                initialValues={{ ...initialValues, dob: moment(initialValues.dob)}}
                scrollToFirstError
            >
                <Form.Item
                    label="name"
                    name="name"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your name!',
                            whitespace: true,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item name="dob" label="Birthday" {...config}>
                    <DatePicker />
                </Form.Item>

                <Form.Item
                    label="city"
                    name="city"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your city!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="State"
                    name="state"
                    rules={[
                        {
                            required: true,
                            message: 'Please input the state!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="Country"
                    name="country"
                    rules={[
                        {
                            required: true,
                            message: 'Please input the country!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="skillset"
                    name="skillset"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your skillset!',
                            whitespace: true,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Career Objective"
                    name="careerObjective"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your career objective!',
                            whitespace: true,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
};

export default BasicForm;
