import React from 'react';
import { useDispatch } from 'react-redux';

import { effects } from '../../../model/experience';
import ExperienceForm from '../components/ExperienceForm';

export default function ExperiencePage () {
    const dispatch = useDispatch();

    const handleCreate = (values) => {
        dispatch(
            effects.createExperience({
                ...values,
            }),
        );
    };

    return (
        <div>
            <ExperienceForm onSubmit={handleCreate} />
        </div>
    );
};

