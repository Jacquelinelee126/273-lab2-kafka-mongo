import React from 'react';
import { Descriptions, Button } from 'antd';
import { Link } from 'react-router-dom';

export default function (props) {
    return props.items.map((ele, index) => (
        <div>
            <Descriptions title={`Work Experience ${index + 1}`} key={ele._id || index}>
                <Descriptions.Item label="Company Name">{ele.companyName}</Descriptions.Item>
                <Descriptions.Item label="Title">{ele.title}</Descriptions.Item>
                <Descriptions.Item label="Location">{ele.location}</Descriptions.Item>
                <Descriptions.Item label="Start Date">{ele.startDate}</Descriptions.Item>
                <Descriptions.Item label="End Date">{ele.endDate}</Descriptions.Item>
            </Descriptions>

            <Button type="primary">
                <Link to={`/student/profile/experience/edit/${ele._id}`}>Edit</Link>
            </Button>
        </div>
    ));
}
