import React from 'react';
import { Form, Input, Button, DatePicker } from 'antd';
import moment from 'moment';

const formItemLayout = {
    labelCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 8,
        },
    },
    wrapperCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 16,
        },
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

const config = {
    rules: [{ type: 'object', required: true, message: 'Please select date!' }],
};

const ExperienceForm = ({ onSubmit, initialValues = {} }) => {
    const [form] = Form.useForm();

    const onFinish = (values) => {
        const { startDate, endDate,...others } = values;

        console.log("valuse", values);
        onSubmit &&
            onSubmit({
                ...others,
                startDate: startDate.format('YYYY-MM-DD'),
                endDate: endDate.format('YYYY-MM-DD')
            });
    };

    return (
        <div className="experience-form">
            <Form
                {...formItemLayout}
                form={form}
                name="experience"
                onFinish={onFinish}
                initialValues={{
                    ...initialValues,
                    startDate: moment(initialValues.startDate),
                    endDate: moment(initialValues.endDate),
                }}
                scrollToFirstError
            >
                <Form.Item
                    label="Company Name"
                    name="companyName"
                    rules={[
                        {
                            required: true,
                            message: 'Please input the company name!',
                            whitespace: true,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="Job Title"
                    name="title"
                    rules={[
                        {
                            required: true,
                            message: 'Please input the job title!',
                            whitespace: true,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Location"
                    name="location"
                    rules={[
                        {
                            required: true,
                            message: 'Please input job location(city, state, country)',
                            whitespace: true,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item name="startDate" label="Start Date" {...config}>
                    <DatePicker />
                </Form.Item>

                <Form.Item name="endDate" label="End Date" {...config}>
                    <DatePicker />
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
};

export default ExperienceForm;
