import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import { message } from 'antd';

import { effects } from '../../../model/experience';
import ExperienceForm from '../components/ExperienceForm';

export default function () {
    const { experience } = useSelector((state) => ({
      experience: state.experience,
    }));

    const { expID } = useParams();
    const dispatch = useDispatch();

    const currentExp = experience.items.find((el) => el._id === expID);
    useEffect(() => {
        if (!currentExp) {
            dispatch(effects.queryExperienceById(expID));
        }
    }, [currentExp, dispatch, expID]);

    const handleUpdate = async (values) => {
        try {
            await dispatch(
                effects.updateExperience(expID, {
                    ...values,
                }),
            );
            message.success('Successfully updated.');
        } catch (e) {
            message.error('Update failed');
        }
    };

    if (experience.loading) {
        return <div>loading...</div>;
    }

    if (experience.error) {
        return <div>error</div>;
    }

    return (
        <div>
            <ExperienceForm initialValues={currentExp} onSubmit={handleUpdate} />
        </div>
    );
}
