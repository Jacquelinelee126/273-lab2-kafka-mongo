/**
 * @file student user profile page
 * @description
 * Display if user has created a profile otherwise
 * allow user to add one by clicking "create" button
 */

import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Button } from 'antd';
import { Link } from 'react-router-dom';

import Basic from './basic/components/Basic';
import Education from './education/components/Education';
import Experience from './experience/components/Experience';

import { effects as BasicEffects } from '../model/basic';
import { effects as EducationEffects } from '../model/education';
import { effects as ExperienceEffects } from '../model/experience';

const Profile = () => {
    const { basic, education, experience } = useSelector((state) => ({
        user: state.user,
        basic: state.basic,
        education: state.education,
        experience: state.experience,
    }));

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(BasicEffects.queryBasic());
        dispatch(EducationEffects.queryEducation());
        dispatch(ExperienceEffects.queryExperiences());
    }, [dispatch]);
    
    return (
        <div className="profile">
            <div className="message">
                <Button type="primary">
                    <Link to="/student/profile/message">Message</Link>
                </Button>
            </div>
            <div className="basic">
                {basic._id ? (
                    <div>
                        <Basic {...basic} />
                        <Button type="primary">
                            <Link to="/student/profile/basic/edit">Edit</Link>
                        </Button>
                    </div>
                ) : (
                    <Button type="primary">
                        <Link to="/student/profile/basic/create">Create</Link>
                    </Button>
                )}
            </div>
            <div className="education">
                {education._id ? (
                    <div>
                        <Education {...education} />
                        <Button type="primary">
                            <Link to="/student/profile/education/edit">Edit</Link>
                        </Button>
                    </div>
                ) : (
                    <Button type="primary">
                        <Link to="/student/profile/education/create">Add Education Experience</Link>
                    </Button>
                )}
            </div>

            <div className="experience">
                <div>
                    <Experience {...experience} />
                </div>
                <br></br>
                <br></br>
                <Button type="primary">
                    <Link to="/student/profile/experience/create">Add Work Experience</Link>
                </Button>
            </div>
        </div>
    );
};

export default Profile;

