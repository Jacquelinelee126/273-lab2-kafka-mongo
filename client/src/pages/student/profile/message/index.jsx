import React from 'react';
import { useDispatch } from 'react-redux';

import { effects } from '../../model/message';
import MessageForm from './component/MessageForm';

export default function MessagePage () {
    const dispatch = useDispatch();

    const handleCreate = (values) => {
        dispatch(
            effects.createMessage({
                ...values,
            }),
        );
    };

    return (
        <div>
            <MessageForm onSubmit={handleCreate} />
        </div>
    );
};

