import React from 'react';
import { Descriptions } from 'antd';
export default function (props) {
    return (
        <div>
            <Descriptions title="Basic">
                <Descriptions.Item label="Name">{props.messages}</Descriptions.Item>
            </Descriptions>
        </div>
    );
}
