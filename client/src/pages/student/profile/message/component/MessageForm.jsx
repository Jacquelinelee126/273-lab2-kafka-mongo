import React from 'react';
import { Form, Input, Button, DatePicker } from 'antd';
import moment from 'moment';

const formItemLayout = {
    labelCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 8,
        },
    },
    wrapperCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 16,
        },
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

const config = {
    rules: [{ type: 'object', required: true, message: 'Please select time!' }],
};

const BasicForm = ({ onSubmit, initialValues = {} }) => {
    const [form] = Form.useForm();

    // console.log('form', form);

    const onFinish = (values) => {
        onSubmit &&
            onSubmit(values);
    };

    return (
        <div className="basic-form">
            <Form
                {...formItemLayout}
                form={form}
                name="basic"
                onFinish={onFinish}
                initialValues={initialValues}
                scrollToFirstError
            >
                <Form.Item
                    label="Message"
                    name="message"
                    rules={[
                        {
                            // required: true,
                            message: 'Please input something!',
                            whitespace: true,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>


                <Form.Item {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">
                        Send
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
};

export default BasicForm;
