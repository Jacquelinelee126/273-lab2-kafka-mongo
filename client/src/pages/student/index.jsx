import React from 'react';
import { Switch, Route, useRouteMatch } from 'react-router-dom';

import BasicLayout from '../../layouts/BasicLayout';

import ProfilePage from './profile';
// import CreateProfilePage from './profile/create';
// import EditProfilePage from './profile/edit';
import ApplicationPage from './application';

import CreateBasicPage from './profile/basic/create';
import CreateEducationPage from './profile/education/create';
import CreateExperiencePage from './profile/experience/create';


import EditBasicPage from './profile/basic/edit';
import EditEducationPage from './profile/education/edit';
import EditExperiencePage from './profile/experience/edit';
import MessagePage from './profile/message'

import OneStudentProfile from './StudentProfile';
import OneJobProfile from './JobProfile';
import OneCompanyProfile from './CompanyProfile';


export default function Routes() {
    const { path } = useRouteMatch();

    return (
        <BasicLayout>
            <Switch>
                <Route exact path={`${path}/profile`}>
                    <ProfilePage />
                </Route>
                <Route exact path={`${path}/profile/basic/create`}>
                    <CreateBasicPage />
                </Route>
                <Route exact path={`${path}/profile/education/create`}>
                    <CreateEducationPage />
                </Route>
                <Route exact path={`${path}/profile/experience/create`}>
                    <CreateExperiencePage />
                </Route>
                <Route exact path={`${path}/profile/basic/edit`}>
                    <EditBasicPage />
                </Route>
                <Route exact path={`${path}/profile/education/edit`}>
                    <EditEducationPage />
                </Route>
                <Route exact path={`${path}/profile/experience/edit/:expID`}>
                    <EditExperiencePage />
                </Route>
                <Route exact path={`${path}/profile/message`}>
                    <MessagePage />
                </Route>
                <Route exact path={`${path}/profile/:sID`}>
                    <OneStudentProfile />
                </Route>
                <Route exact path={`${path}/job/:jID`}>
                    <OneJobProfile />
                </Route>
                <Route exact path={`${path}/company/:company`}>
                    <OneCompanyProfile />
                </Route>
                {/* <Route path={`${path}/profile/create`}>
                    <CreateProfilePage />
                </Route>
                <Route path={`${path}/profile/edit`}>
                    <EditProfilePage /> */}
                {/* </Route> */}
                <Route exact path={`${path}/application`}>
                    <ApplicationPage />
                </Route>
                <Route render={() => <div>404 Not Found</div>} />
            </Switch>
        </BasicLayout>
    );
}
