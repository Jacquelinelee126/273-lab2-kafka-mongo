/**
 * @file Company user profile page
 * @description
 * Display if user has created a profile otherwise
 * allow user to add one by clicking "create" button
 */
import { message, Button } from 'antd';
import { Link, useParams } from 'react-router-dom';

import React, { useEffect } from 'react';
import { useState } from 'react';

import ProfileBasic from '../company/profile/components/ProfileBasic';

import * as api from '../search/service';

const OneCompanyProfile = () => {
    const { company } = useParams();

    const [currentCompany, setCurrentCompany] = useState();
    // const [loading, setLoading] = useState(false); // loading

    // try catch
    useEffect(() => {
        async function listOneCompany() {
            try {
                const currentCompany = await api.queryOneCompany(company);
                setCurrentCompany(currentCompany);
                // setLoading(true);
            } catch (e) {
                message.error(e);
                // setLoading(false);
            }
        }
        listOneCompany();
    }, []);

    // if (loading) {
    //     return <div>loading...</div>;
    // }
    // if (!loading) {
    //     return <div>Error...</div>;
    // }

    console.log('currentCompany', currentCompany);
    if (!currentCompany || Object.keys(currentCompany).length === 0) {
        return <h1>This Company no detailed information!</h1>;
    }
    // reuse component and check user id

    return (
        <div>
            <ProfileBasic {...currentCompany}></ProfileBasic>
        </div>
    );
};

export default OneCompanyProfile;
