import React from "react";
import { useDispatch } from "react-redux";

import { effects } from "../../model/job";
import JobForm from "../components/JobForm";

export default function() {

  const dispatch = useDispatch();

  const handleCreate = values => {
    dispatch(
      effects.createJob({
        ...values
      })
    );
  };

  return (
    <div>
      <JobForm onSubmit={handleCreate} />
    </div>
  );
}