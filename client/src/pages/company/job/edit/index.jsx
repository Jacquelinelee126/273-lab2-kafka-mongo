import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { message } from 'antd';
import { useParams } from 'react-router-dom';

import { effects } from '../../model/job';
import JobForm from '../components/JobForm';

const EditJobPage = () => {
    const { job } = useSelector((state) => ({
        job: state.job,
    }));

    // Filter job
    console.log('jobList====>', job);
    const { jID } = useParams();

    const currentJob = job.list.find((el) => el._id === jID);
    console.log('Params: ===>', jID);
    console.log('currentJob ===> ', currentJob);
    const dispatch = useDispatch();

    useEffect(() => {
        if (!currentJob) {
            dispatch(effects.queryJobById(jID));
        }
    }, [currentJob, dispatch, jID]);

    const handleUpdate = async (values) => {
        try {
            await dispatch(
                effects.updateJob(jID, {
                    ...values,
                }),
            );
            message.success('Update successfully!');
        } catch (e) {}
    };

    if (job.loading) {
        return <div>loading...</div>;
    }

    if (job.error) {
        return <div>error</div>;
    }

    return (
        <div>
            <JobForm initialValues={currentJob} onSubmit={handleUpdate} />
        </div>
    );
};

export default EditJobPage;
