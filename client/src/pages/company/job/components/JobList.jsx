import React from "react";
import { Table, Divider } from "antd";
import { Link } from "react-router-dom";

export default function(props) {
  // const user = useSelector(state => state.user);
  const columns = [
    {
      title: "Title",
      dataIndex: "title",
      key: "title"
    },
    {
      title: "Location",
      dataIndex: "location",
      key: "location"
    },
    {
      title: "Salary",
      dataIndex: "salary",
      key: "salary"
    },
    {
      title: "Description",
      dataIndex: "jobDescription",
      key: "jobDescription"
    },
    {
      title: "Application Dealine",
      dataIndex: "applicationDeadline",
      key: "applicationDealine"
    },
    {
      title: "Actions",
      key: "action",
      render: (record) => (
        <span>
          <Link to={`/company/job/${record._id}`}>modify</Link>
          <Divider type="vertical" />
          <Link to={`/company/job/${record._id}/application`}>applications</Link>
        </span>
      )
    }
  ];

  return (
    <Table
     rowKey="_id"
      dataSource={props.list}
      columns={columns}
      pagination={true}
    />
  );
};