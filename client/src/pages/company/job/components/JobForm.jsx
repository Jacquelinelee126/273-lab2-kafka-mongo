import React from 'react';
import { Form, Input, Select, InputNumber, Button, DatePicker } from 'antd';
import moment from 'moment';

const formItemLayout = {
    labelCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 8,
        },
    },
    wrapperCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 16,
        },
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

const JobForm = ({ onSubmit, initialValues = {} }) => {
    const [form] = Form.useForm();

    const onFinish = (values) => {
        const { applicationDeadline, ...others } = values;

        onSubmit &&
            onSubmit({
                ...others,
                applicationDeadline: applicationDeadline.format('YYYY-MM-DD'),
            });
    };

    console.log('date ==>', initialValues.applicationDeadline);

    return (
        <div className="register-form">
            <Form
                {...formItemLayout}
                form={form}
                name="company_profile"
                onFinish={onFinish}
                initialValues={{
                    ...initialValues,
                    applicationDeadline: moment(initialValues.applicationDeadline),
                }}
                scrollToFirstError
            >
                <Form.Item
                    label="title"
                    name="title"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your title!',
                            whitespace: true,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Category"
                    name="category"
                    rules={[
                        {
                            required: true,
                            message: 'Please input job category!',
                            whitespace: true,
                        },
                    ]}
                >
                    <Select>
                        <Select.Option value="full_time">Full Time</Select.Option>
                        <Select.Option value="part_time">Part Time</Select.Option>
                        <Select.Option value="intern">Intern</Select.Option>
                        <Select.Option value="on_campus">On Campus</Select.Option>
                    </Select>
                </Form.Item>

                <Form.Item
                    label="salary"
                    name="salary"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your salary!',
                        },
                    ]}
                >
                    <InputNumber />
                </Form.Item>

                <Form.Item
                    label="location"
                    name="location"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your location!',
                            whitespace: true,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Job Description"
                    name="jobDescription"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your job_description!',
                            whitespace: true,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Application Deadline"
                    name="applicationDeadline"
                    rules={[
                        {
                            required: true,
                        },
                    ]}
                >
                    <DatePicker />
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
};

export default JobForm;
