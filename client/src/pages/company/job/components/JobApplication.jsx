import React, { useState } from 'react';
// import { Link} from 'react-dom';
import { Table, Button, message } from 'antd';
import { useDispatch } from 'react-redux';

import { effects } from '../../model/job';
import { Link } from "react-router-dom";


import ApplicationStatusForm from './ApplicationStatusForm';

export default function (props) {
    console.log("props,", props.list);
    const [visible, setVisible] = useState(false);
    const [current, setCurrent] = useState();
    const dispatch = useDispatch();

    const columns = [
        // {
        //     title: 'id',
        //     dataIndex: 'id',
        //     key: 'id',
        // },
        {
            title: 'Applicant',
            dataIndex: 'student',
            key: 'applicant',
            // render: (company) =>
                // company ? (
                //     <Link to={`/student/company/${company.name}`}>{company.name} </Link>
                // ) : (
                //     'N/A'
                // ),
            render: (student) => student ? <Link to={`/student/profile/${student.owner}`}>{student.name}</Link> : 'N/A'
        },

        {
            title: 'status',
            dataIndex: 'status',
            key: 'status',
        },
        {
            title: 'Action',
            key: 'action',
            render: (text, record) => (
                <span>
                    <Button
                        onClick={() => {
                            setCurrent(record);
                            setVisible(true);
                        }}
                    >
                        Change Status
                    </Button>
                </span>
            ),
        },
    ];

    const handleSubmit = async (values) => {
        console.log('handleSubmit:', current);
        console.log('current:');
        const { _id } = current;

        try {
            await dispatch(effects.changeApplicationStatus(_id, values));
            message.success('change success');
        } catch (e) {
            message.error(e);
        }
    };

    const handleCancel = () => {
        setVisible(false);
        setCurrent(null);
    };

    return (
        <>
            <Table rowKey="id" dataSource={props.list} columns={columns} pagination={false} /> 
            <ApplicationStatusForm
                visible={visible}
                initial={current}
                onSubmit={handleSubmit}
                onCancel={handleCancel}
            /> 

            {/* <Pagination
                size="large"
                total={total}
                showTotal={(total) => `Total ${total} applications`}
                defaultCurrent={1}
                defaultPageSize={2}
                showQuickJumper={true}
                showSizeChanger={true}
                pageSizeOptions={[1, 2, 5, 10, 20]}
                onChange={onChange}
                onShowSizeChange={onChange}
            /> */}
        </>
    );
}
