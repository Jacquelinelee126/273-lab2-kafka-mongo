import React from "react";
import { Descriptions } from "antd";

export default function(props) {
  return (
    <Descriptions title="Job Detail">
      <Descriptions.Item label="Name">{props.title}</Descriptions.Item>
      <Descriptions.Item label="Location">{props.location}</Descriptions.Item>
      <Descriptions.Item label="Salary">{props.salary}</Descriptions.Item>
      <Descriptions.Item label="Description">{props.jobDescription}</Descriptions.Item>
      <Descriptions.Item label="Deadline">{props.applicationDeadline}</Descriptions.Item>
      <Descriptions.Item label="Category">{props.category}</Descriptions.Item>
    </Descriptions>
  );
}