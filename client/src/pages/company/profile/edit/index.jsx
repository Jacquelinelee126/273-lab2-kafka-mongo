import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { message } from 'antd';

import { effects } from '../../model';
import ProfileForm from '../components/ProfileForm';

export default function () {
    const { _id, company } = useSelector((state) => ({
        _id: state.user._id,
        company: state.company,
    }));
    const dispatch = useDispatch();

    useEffect(() => {
        if (_id && !company._id) {
            dispatch(effects.queryProfile());
        }
    }, [dispatch, _id, company._id]);

    const handleUpdate = async (values) => {
        try {
            await dispatch(
                effects.updateProfile({
                    ...values,
                }),
            );
            message.success('Updated successfully');
        } catch (e) {}
    };

    if (company.loading) {
        return <div>loading...</div>;
    }

    if (company.error) {
        return <div>error</div>;
    }

    return (
        <div>
            <ProfileForm initialValues={company} onSubmit={handleUpdate} />
        </div>
    );
}
