import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Button, Empty } from 'antd';
import { Link } from 'react-router-dom';

import ProfileBasic from './components/ProfileBasic';

import { effects } from '../model';

const Profile = () => {
    const { company } = useSelector((state) => ({
        // user: state.user,
        company: state.company,
    }));
    const dispatch = useDispatch();

    useEffect(() => {
        if (!company._id) {
            dispatch(effects.queryProfile());
        }
    }, [dispatch, company._id]); // effect dependency

    if (company.loading) {
        return <div>loading...</div>;
    }

    if (company.error) {
        return <div>error</div>;
    }

    if (!company._id) {
        return (
            <div>
                <Empty
                    image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
                    imageStyle={{
                        height: 60,
                    }}
                    description={
                        <span>
                           It looks like your profile is empty.
                        </span>
                    }
                >
                    <Button type="primary">
                        <Link to="/company/profile/create">Create Now!</Link>
                    </Button>
                </Empty>
                {/* <h3> It looks like your profile is empty. Please create it!</h3>
                <Button type="primary">
                    <Link to="/company/profile/create">Create</Link>
                </Button> */}
            </div>
        );
    }

    return (
        <div>
            <ProfileBasic {...company} />
            {/* <ProfileBasic {...company} /> */}
            <Button type="primary">
                <Link to="/company/profile/edit">Edit</Link>
            </Button>
        </div>
    );
};

export default Profile;
