import React from "react";
import { useDispatch } from "react-redux";

import { effects } from "../../model";
import ProfileForm from "../components/ProfileForm";

export default function() {
  const dispatch = useDispatch();

  const handleCreate = values => {
    dispatch(
      effects.createProfile({
        ...values
      })
    );
  };

  return (
    <div>
      <ProfileForm onSubmit={handleCreate} />
    </div>
  );
}