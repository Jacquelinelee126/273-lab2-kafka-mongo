import React from 'react';
import { Switch, Route, useRouteMatch } from 'react-router-dom';

import BasicLayout from '../../layouts/BasicLayout';

import ProfilePage from './profile';
import CreateProfilePage from './profile/create';
import EditProfilePage from './profile/edit';
import JobPage from './job';
import CreateJobPage from './job/create';
import EditJobPage from './job/edit';
import JobApplicationsPage from './job/applications';

import EventPage from './event';
import CreateEventPage from './event/create';
import EditEventPage from './event/edit';
import EventRegistrationPage from './event/registrations';

export default function Routes() {
    const { path } = useRouteMatch();

    return (
        <BasicLayout>
            <Switch>
                {/* company profile */}
                <Route exact path={`${path}/profile`}>
                    <ProfilePage />
                </Route>
                <Route path={`${path}/profile/create`}>
                    <CreateProfilePage />
                </Route>
                <Route path={`${path}/profile/edit`}>
                    <EditProfilePage />
                </Route>
                {/* all jobs */}
                <Route exact path={`${path}/job`}>
                    <JobPage />
                </Route>
                <Route exact path={`${path}/job/create`}>
                    <CreateJobPage />
                </Route>
                <Route exact path={`${path}/job/:jID`}>
                    <EditJobPage />
                </Route>
                {/* one job related */}
                <Route exact path={`${path}/job/:jID/application`}>
                    <JobApplicationsPage />
                </Route>
                <Route exact path={`${path}/event`}>
                    <EventPage />
                </Route>
                <Route exact path={`${path}/event/create`}>
                    <CreateEventPage />
                </Route>
                <Route exact path={`${path}/event/edit/:eID`}>
                    <EditEventPage />
                </Route>
                <Route exact path={`${path}/event/:eID/registration`}>
                    <EventRegistrationPage />
                </Route>
                <Route render={() => <div>404 Not Found</div>} />
            </Switch>
        </BasicLayout>
    );
}
