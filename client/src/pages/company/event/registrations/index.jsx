/**
 * @file List of applicatoins for one event
 * @description
 */
import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Button } from 'antd';
import { Link, useParams } from 'react-router-dom';

import EventRegistration from '../components/EventRegistration';

import { effects } from '../../model/event';

const EventRegistrationPage = () => {
    console.log("registration Page");
    const { eID } = useParams();

    const { event, registration } = useSelector((state) => ({
        event: state.event,
        registration: state.event.registration,
    }));

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(effects.queryRegistration(eID));
    }, [dispatch, eID]);

    if (event.loading) {
        return <div>loading...</div>;
    }

    if (event.error) {
        return <div>error</div>;
    }

    if (registration.length === 0) {
        return (
            <div>
                <h3> This event has no registration!</h3>
                <Button type="primary">
                    <Link to="/company/event">Go Back</Link>
                </Button>
            </div>
        );
    }

    return (
        <div>
            {/* registration list*/}
            <EventRegistration list={registration} eID={eID} />
        </div>
    );
};

export default EventRegistrationPage;
