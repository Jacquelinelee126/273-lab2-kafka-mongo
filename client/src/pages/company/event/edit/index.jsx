import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { message } from 'antd';
import { useParams } from 'react-router-dom';

import { effects } from '../../model/event';
import EventForm from '../components/EventForm';

const EditEventPage = () => {
    const { event } = useSelector((state) => ({
        event: state.event,
    }));

    // Filter event
    console.log('eventList====>', event.list);
    const { eID } = useParams();

    const currentEvent = event.list.find((el) => el._id === eID);
    console.log('currentevent ===> ', currentEvent);
    const dispatch = useDispatch();

    useEffect(() => {
        if (!currentEvent) {
            dispatch(effects.queryEventById(eID));
        }
    }, [currentEvent, dispatch, eID]);

    const handleUpdate = async (values) => {
        try {
            await dispatch(
                effects.updateEvent(eID, {
                    ...values,
                }),
            );
            message.success('Update successfully!');
        } catch (e) {}
    };

    if (event.loading) {
        return <div>loading...</div>;
    }

    if (event.error) {
        return <div>error</div>;
    }

    return (
        <div>
            <EventForm initialValues={currentEvent} onSubmit={handleUpdate} />
        </div>
    );
};

export default EditEventPage;
