
import React, { useState, useEffect, useParams} from 'react';
import { Table, Button } from "antd";
import { useDispatch } from "react-redux";

// import { effects } from "../../model/event";

// import { useSelector } from "react-redux";

// import SearchForm from './SearchForm';
// import BasicLayout from '../../../layouts/BasicLayout';

import * as api from '../../model/service';
import Education from '../../../student/profile/education/components/Education';

// import ApplicationStatusForm from "./ApplicationStatusForm";

export default function(props) {
  const [registrations, setEventRegistration] = useState([]);
  const [visible, setVisible] = useState(false);
  const [current, setCurrent] = useState(null);
  const dispatch = useDispatch();

  useEffect(() => {
    async function listAllRegistration() {
        const registrations = await api.queryEventRegistrations(props.eID);
        setEventRegistration(registrations);
    }
    listAllRegistration();
}, []);

  const columns = [
    // {
    //   title: "Event ID",
    //   dataIndex: "_id",
    //   key: "_id"
    // },
    {
      title: "Applicant",
      dataIndex: "basic",
      key: "applicant",
      render: (basic) => (basic ? basic.name : 'N/A'),
    },
    {
      title: "College",
      dataIndex: "education",
      key: "college",
      render: (education) => (education ? education.collegeName : 'N/A'),
    },
    {
      title: "Major",
      dataIndex: "education",
      key: "major",
      render: (education) => (education ? education.major : 'N/A'),
    },
    // {
    //   title: "status",
    //   dataIndex: "status",
    //   key: "status"
    // },
  ];

  // const handleSubmit = async values => {
  //   console.log("handleSubmit:", values);
  //   const { uid, jid } = props;
  //   const { id: aid } = current;

  //   try {
  //     await dispatch(effects.changeApplicationStatus(uid, jid, aid, values));
  //     message.success("change success");
  //   } catch (e) {}
  // };

  // const handleCancel = () => {
  //   setVisible(false);
  //   setCurrent(null);
  // };

  return (
    <>
      <Table
        rowKey="id"
        dataSource={registrations}
        columns={columns}
        pagination={true}
      />
      {/* <ApplicationStatusForm
        visible={true}
        initial={current}
        onSubmit={handleSubmit}
        onCancel={handleCancel}
      /> */}
    </>
  );
}