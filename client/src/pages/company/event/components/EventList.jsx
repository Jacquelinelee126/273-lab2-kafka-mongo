import React from 'react';
import { Table, Divider, Button } from 'antd';
import { Link } from 'react-router-dom';

export default function (props) {
    // const user = useSelector(state => state.user);
    const columns = [
        {
            title: 'Title',
            dataIndex: 'title',
            key: 'title',
        },
        {
            title: 'Location',
            dataIndex: 'location',
            key: 'location',
        },
        {
            title: 'Major',
            dataIndex: 'major',
            key: 'major',
        },
        {
            title: 'Description',
            dataIndex: 'description',
            key: 'description',
        },
        {
            title: 'Time',
            dataIndex: 'time',
            key: 'time',
        },
        {
            title: 'Actions',
            key: 'action',
            render: (record) => (
                <span>
                    <Link to={`/company/event/edit/${record._id}`}>Modify</Link>
                    <Divider type="vertical" />
                    <Link to={`/company/event/${record._id}/registration`}>Registrations</Link>
                </span>
            ),
        },
    ];

    return (
        <div>
            <Table rowKey="_id" dataSource={props.list} columns={columns} pagination={true} />
            <Button type="primary">
                <Link to="/company/event/create">Create Another Event</Link>
            </Button>
        </div>
    );
}
