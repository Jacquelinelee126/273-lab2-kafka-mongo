import React from 'react';
import { Form, Input, Select, Button, DatePicker } from 'antd';
import moment from 'moment';

const formItemLayout = {
    labelCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 8,
        },
    },
    wrapperCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 16,
        },
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

const EventForm = ({ onSubmit, initialValues = {} }) => {
    const [form] = Form.useForm();

    const onFinish = (values) => {
        const { time, ...others } = values;

        onSubmit && onSubmit({ ...others, time: time.format('YYYY-MM-DD HH:mm') });
    };

    return (
        <div className="register-form">
            <Form
                {...formItemLayout}
                form={form}
                name="company_profile"
                onFinish={onFinish}
                initialValues={{...initialValues,
                time: moment(initialValues.time)}}
                scrollToFirstError
            >
                <Form.Item
                    label="Title"
                    name="title"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your title!',
                            whitespace: true,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Major"
                    name="major"
                    rules={[
                        {
                            required: true,
                            message: 'Please input major!',
                            whitespace: true,
                        },
                    ]}
                >
                    <Select>
                        <Select.Option value="Software Engineering">Software Engineering</Select.Option>
                        <Select.Option value="Electrical Engineering">Electrical Engineering</Select.Option>
                        <Select.Option value="ommunications">Communications</Select.Option>
                        <Select.Option value="Business">Business</Select.Option>
                        <Select.Option value="Economics">Economics</Select.Option>
                        <Select.Option value="Literature">Literature</Select.Option>
                        <Select.Option value="Psychology">Psychology</Select.Option>
                        <Select.Option value="Nursing">Nursing</Select.Option>
                        <Select.Option value="Computer Science">Computer Science</Select.Option>
                        <Select.Option value="All">All</Select.Option>

                    </Select>
                </Form.Item>

                <Form.Item
                    label="Location"
                    name="location"
                    rules={[
                        {
                            required: true,
                            message: 'Please input the event location!',
                        },
                    ]}
                >
                <Input />
                </Form.Item>

                <Form.Item
                    label="Description"
                    name="description"
                    rules={[
                        {
                            required: true,
                            message: 'Please describe the event!',
                            whitespace: true,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Time"
                    name="time"
                    rules={[
                        {
                            required: true,
                        },
                    ]}
                >
                    <DatePicker showTime={{ format: 'HH:mm' }} format="YYYY-MM-DD HH:mm" />
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
};

export default EventForm;
