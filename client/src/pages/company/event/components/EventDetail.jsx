import React from "react";
import { Descriptions } from "antd";

export default function(props) {
  return (
    <Descriptions title="Event Detail">
      <Descriptions.Item label="Title">{props.title}</Descriptions.Item>
      <Descriptions.Item label="Location">{props.location}</Descriptions.Item>
      <Descriptions.Item label="Description">{props.description}</Descriptions.Item>
      <Descriptions.Item label="Event Time">{props.time}</Descriptions.Item>
      <Descriptions.Item label="Major allowed:">{props.major}</Descriptions.Item>
    </Descriptions>
  );
}