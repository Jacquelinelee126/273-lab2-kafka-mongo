import React from 'react';
import { useDispatch } from 'react-redux';

import { effects } from '../../model/event';
import EventForm from '../components/EventForm';

export default function () {
    const dispatch = useDispatch();

    const handleCreate = (values) => {
        dispatch(
            effects.createEvent({
                ...values,
            }),
        );
    };

    return (
        <div>
            <EventForm onSubmit={handleCreate} />
        </div>
    );
}
