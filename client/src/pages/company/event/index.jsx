import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Button } from 'antd';
import { Link } from 'react-router-dom';

import EventList from './components/EventList';

import { effects } from '../model/event';

const EventPage = () => {
    const { event } = useSelector((state) => ({
        // id: state.user.id,
        event: state.event,
    }));
    const dispatch = useDispatch();

    useEffect(() => {
        if (event.list.length === 0) {
            dispatch(effects.queryEvents());
        }
    }, [dispatch, event.list.length]);

    if (event.loading) {
        return <div>loading...</div>;
    }

    if (event.error) {
        return <div>error</div>;
    }

    if (event.length === 0) {
        return (
            <div>
                <h3> You don't have any events, please create one!</h3>
                <Button type="primary">
                    <Link to="/company/event/create">Create</Link>
                </Button>
            </div>
        );
    }

    return (
        <div>
            {/* event list*/}
            {/* key={number.toString()} */}
            <EventList list={event.list} />
        </div>
    );
};

export default EventPage;
