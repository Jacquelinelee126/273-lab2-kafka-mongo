import request from '../../../utils/request';

// profile related
export const queryProfile = (_) => {
    return request.get(`/company/`);
};

export const createProfile = (data) => {
    return request.post(`/company`, data);
};

export const updateProfile = (data) => {
    return request.put(`/company/`, data);
};

export const queryJobs = () => {
    return request.get(`/company/job`);
};

export const queryJobById = (jID) => {
    return request.get(`/job/${jID}`);
};

export const createJob = (data) => {
    return request.post(`/job`, data);
};

export const updateJob = (jID, data) => {
    return request.put(`/job/${jID}`, data);
};

export const queryJobApplications = (jID) => {
    return request.get(`/company/job/${jID}/application`);
};

export const updateJobApplicationStatus = (aID, data) => {
    return request.put(`/company/application/${aID}`, data);
};

export const queryEvents = () => {
    return request.get(`/company/event`);
};

export const queryEventById = (eID) => {
    return request.get(`/event/${eID}`);
};

export const createEvent = (data) => {
    return request.post(`/event`, data);
};
export const updateEvent = (eID,  data) => {
    return request.put(`/event/${eID}`, data);
};

export const queryEventRegistrations = (eID) => {
    return request.get(`/company/event/${eID}/registration`);
};


