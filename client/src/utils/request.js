import axios from 'axios';
import { message } from 'antd';

import { history } from './history';

import { getToken } from '.';

const fetchInstance = axios.create({
    baseURL: '/api/',
    withCredentials: true,
});

// interceptors for all requests
fetchInstance.interceptors.request.use(
    (config) => {
        config.headers['Authorization'] = 'Bearer ' + getToken();
        return config;
    },
    (error) => {
        Promise.reject(error);
    },
);

fetchInstance.interceptors.response.use(
    (response) => {
        console.log('response', response);
        const { data } = response;
        if (data.token) {
            localStorage.setItem('handShakeToken', data.token);
        }
        if (data.error) {
            throw data.data;
        }

        return data.data;
    },
    (error) => {
        const { response } = error;
        const { data } = response;

        message.error('network error::' + response);

        message.error('network error::' + error);
        console.log('data::' + data);

        if (response.status === 401) {
            message.error('Please login to access this website.');
            history.push(`/user/login?redirect=${window.location}`);
        }
        throw error;
    },
);

export default fetchInstance;
