import React from "react";
import { Route, Switch, Redirect} from "react-router"; 
//import { BrowserRouter } from "react-router-dom";

import UserPage from "../../pages/user";
import HomePage from "../../pages/home";
import StudentPage from "../../pages/student";
import CompanyPage from "../../pages/company";
import SearchJobPage from "../../pages/search/job";
import SearchEventPage from "../../pages/search/event";
import SearchStudentPage from "../../pages/search/student";


export default function SiteRoute(){
    return (
        <Switch>
            <Route path="/home">
            <HomePage />
            </Route>
            <Route path="/user">
            <UserPage />
            </Route>
            <Route path="/student">
            <StudentPage />
            </Route>
            <Route path="/company">
            <CompanyPage />
            </Route>
            <Route exact path="/search/job">
            <SearchJobPage />
            </Route>
            <Route exact path="/search/event">
            <SearchEventPage />
            </Route>
            <Route exact path="/search/student">
            <SearchStudentPage />
            </Route>
            <Redirect to="/home"/>
        </Switch>
    );
}