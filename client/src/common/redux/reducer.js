import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import { history } from '../../utils/history';
import { reducer as globalReducer } from './model';

import { reducer as userReducer } from '../../pages/user/model';
import { reducer as studentReducer } from '../../pages/student/model';
import { reducer as companyReducer } from '../../pages/company/model';
import { reducer as jobReducer } from '../../pages/company/model/job';
import { reducer as eventReducer } from '../../pages/company/model/event';
import { reducer as basicReducer } from '../../pages/student/model/basic';
import { reducer as educationReducer } from '../../pages/student/model/education';
import { reducer as experienceReducer } from '../../pages/student/model/experience';

// root reducer creator
const createRootReducer = (history) =>
    combineReducers({
        router: connectRouter(history),
        global: globalReducer,
        user: userReducer,
        student: studentReducer,
        company: companyReducer,
        job: jobReducer,
        event: eventReducer,
        basic: basicReducer,
        education: educationReducer,
        experience: experienceReducer,
    });

// create the reducer with history
// root reducer with router state
const reducer = createRootReducer(history);

export default reducer;
