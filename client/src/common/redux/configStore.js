import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';

import { routerMiddleware } from 'connected-react-router';
import { history } from '../../utils/history';

import rootReducer from './reducer';
// routerMiddleware(history) to dispatch history actions (e.g. to change URL with push('/path/to/somewhere')).
// default middleware includes 
// thunk: true;
// immutableCheck: true;
// serializableCheck: true;
const middleware = [routerMiddleware(history), ...getDefaultMiddleware()];

// enhancers add extra functionality to the Redux store
// Not using any enhancers here
const enhancers = [];

// createStore() makes extensibility hard to maintain
export default function configureAppStore(preloadedState) {
    const store = configureStore({
        reducer: rootReducer,
        middleware,
        preloadedState,
        enhancers,
    });

    if (process.env.NODE_ENV !== 'production' && module.hot) {
        module.hot.accept('./reducer', () => store.replaceReducer(rootReducer));
    }

    return store;
}
