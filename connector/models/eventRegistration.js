const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const eventRegistrationSchema = new mongoose.Schema(
  {
    applicant: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'User',
    },
    event: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'Event',
    },
  },
  {
    timestamps: true,
  },
);

eventRegistrationSchema.index({ applicant: 1, event: 1 }, { unique: true });
// Plugins
eventRegistrationSchema.plugin(uniqueValidator, {
    message : 'Compound Unique'
  })
const EventRegistration = mongoose.model('EventRegistration', eventRegistrationSchema);
module.exports = EventRegistration;
