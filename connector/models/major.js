const mongoose = require('mongoose');
const validator = require('validator');

const majorSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true,
    },
});

// "name": 'Software Engineering'
// "name": 'Electrical Engineering'
// "name": 'Communications')
// "name": 'Business'
// "name": 'Economics'
// "name": 'Literature'
// "name": 'Psychology'
// "name": 'Education'
// "name": 'Nursing'
// "name": 'Computer Science'
// "name": 'All'

const Major = mongoose.model('Major', majorSchema);

module.exports = Major;
