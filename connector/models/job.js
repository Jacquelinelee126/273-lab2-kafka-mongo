const mongoose = require('mongoose');
// const validator = require('validator');

const jobSchema = new mongoose.Schema(
    {
        owner: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'User',
        },
        title: {
            type: String,
            required: true,
            // lowercase: true
        },
        location: {
            type: String,
            required: true,
        },
        salary: {
            type: String,
            required: true,
        },
        jobDescription: {
            type: String,
            required: true,
        },
        applicationDeadline: {
            type: String,
            required: true,
        },
        category: {
            type: String,
            required: true,
            enum: ['full_time', 'part_time', 'intern', 'on_campus'],
        },
    },
    {
        timestamps: true,
    },
);

const Job = mongoose.model('Job', jobSchema);
module.exports = Job;
