const mongoose = require('mongoose');

const basicSchema = new mongoose.Schema({
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        unique: true,
        ref: 'User',
    },
    name: {
        type: String,
        required: true,
        trim: true,
    },
    dob: {
        type: Date,
        required: true,
    },
    city: {
        type: String,
        required: true,
        trim: true,
    },
    state: {
        type: String,
        required: true,
        trim: true,
    },
    country: {
        type: String,
        required: true,
        trim: true,
    },
    skillset: {
        type: String,
        required: true,
        trim: true,
    },
    careerObjective: {
        type: String,
        required: true,
    },
});

const Basic = mongoose.model('Basic', basicSchema);
module.exports = Basic;
