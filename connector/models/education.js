const mongoose = require('mongoose');
const educationSchema = new mongoose.Schema({
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        unique: true,
        ref: 'User',
    },
    collegeName: {
        type: String,
        required: true,
    },
    location: {
        type: String,
        required: true,
    },
    degree: {
        type: String,
        required: true,
    },
    major: {
        type: String,
        required: true,
    },
    yearOfPassing: {
        type: String,
        required: true,
    },
});

const Education = mongoose.model('Education', educationSchema);
module.exports = Education;
