const mongoose = require('mongoose');

const eventSchema = new mongoose.Schema({
  owner: {
    type:mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  }, 
  title: {
    type: String,
    required: true,
    trim: true,
    lowercase:true
  },
  location: {
    type: String,
    required: true,
    trim: true,
  },
  description: {
    type: String,
    required: true,
  },
  time: {
    type: Date,
    required: true,
  },
  major: {
    type: String,
    required: true,
    ref: 'Major',
  },
});

const Event = mongoose.model('Event', eventSchema);
module.exports = Event;