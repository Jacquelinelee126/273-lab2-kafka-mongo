const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const jobApplicationSchema = new mongoose.Schema(
    {
        applicant: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'User',
        },
        job: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'Job'
        },
        resume: {
            type: Buffer,
            required:true
        },
        status: {
            type: String,
            default: 'pending',
            enum: ['pending', 'reviewed', 'declined', 'approved'],
        },
    },
    {
        timestamps: true,
    },
);

jobApplicationSchema.index({ applicant: 1, job: 1 }, { unique: true });
// Plugins
jobApplicationSchema.plugin(uniqueValidator, {
    message : 'Compound Unique'
  })
const JobApplication = mongoose.model('JobApplication', jobApplicationSchema);
module.exports = JobApplication;
