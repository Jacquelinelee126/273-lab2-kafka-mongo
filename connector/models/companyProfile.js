const mongoose = require('mongoose');
const validator = require('validator');
// const { jobSchema } = require('./job');
// const { eventSchema } = require('./event');

const companyProfileSchema = new mongoose.Schema(
    {
        owner: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            unique: true,
            ref: 'User',
        },
        avatar: {
            type: Buffer,
        },
        name: {
            type: String,
            required: true,
            unique: true,
            trim: true,
        },
        location: {
            type: String,
            required: true,
            trim: true,
            lowercase: true,
        },
        description: {
            type: String,
            required: true,
            trim: true,
            lowercase: true,
        },
        email: {
            type: String,
            required: true,
            unique: true,
            trim: true,
            lowercase: true,
            validator(value) {
                if (!validator.isEmail(value)) {
                    throw new Error('Email is invalid');
                }
            },
        },
        phone: {
            type: String,
            required: true,
        },
        // jobs: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Job' }]
    },
    {
        timestamps: true,
    },
);

const CompanyProfile = mongoose.model('CompanyProfile', companyProfileSchema);
module.exports = CompanyProfile;
