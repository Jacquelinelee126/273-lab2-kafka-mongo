const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcrypt');
const { secretKey } = require('../constants');
const jwt = require('jsonwebtoken');

const userSchema = new mongoose.Schema(
    {
        username: {
            type: String,
            required: true,
            trim: true,
        },
        email: {
            type: String,
            required: true,
            trim: true,
            unique: true,
            lowercase: true,
            validator(value) {
                if (!validator.isEmail(value)) {
                    throw new Error('Email is invalid');
                }
            },
        },
        password: {
            type: String,
            required: true,
            minlength: 7,
            trim: true,
        },
        avatar: {
            type: Buffer,
        },
        resume: {
            type: Buffer,
        },
        role: {
            type: String,
            enum: ['student', 'company'],
        },
    },
    {
        timestamps: true,
    },
);

userSchema.virtual('jobs', {
  ref: 'Task',
  localField: '_id',
  foreignField: 'owner'
})

userSchema.statics.findByCredentials = async (email, password) => {
    const user = await User.findOne({ email });

    if (!user) {
        return new Error('Unable to login. Invalid email');
    }

    const isMatch = await bcrypt.compare(password, user.password);

    if (!isMatch) {
        return new Error('Unable to login. Invalid credentials');
    }

    return user;
};

userSchema.methods.generateAuthToken = async function() {
    const user = this;

    const token = jwt.sign({ _id: user._id.toString(), role: user.role }, secretKey);
    return token;
};
// before save, we hash
userSchema.pre('save', async function(next) {
    const user = this;

    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 10);
    }

    next();
});

const User = mongoose.model('User', userSchema);

module.exports = User;
