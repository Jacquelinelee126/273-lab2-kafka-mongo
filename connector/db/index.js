const mongoose = require('mongoose');

mongoose.connect("mongodb://127.0.0.1:27017/handshake",{
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false
})
.then((result) => console.log("Success!"))
.catch((error) => console.log("Fail!"))
