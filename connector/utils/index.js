module.exports.STATUS_CODE = {
    SUCCESS: 200,
    CREATE_SUCCESSFULLY: 201,
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    NOT_FOUND: 404,
    INTERNER_SERVER_ERROR: 500,
};

module.exports.MESSAGES = {
    ACTION_NOT_COMPLETE: 'Action is not complete. Please try again later',
    DATA_NOT_FOUND: "Data doesn't exist",
    DATA_ALREADY_EXIST: 'Data already exist',
    SUCCESS: 'Ok',
    CREATE_SUCCESSFULLY: 'Created successfully',
    UPDATE_SUCCESSFULLY: 'Updated successfully',
    DELETE_SUCCESSFULLY: 'Delete successfully',
    INTERNER_SERVER_ERROR: 'Interner server error',
    INVALID_INPUT: 'Invalid input',
    USER_ALREADY_EXIT: 'User already exist',
};
