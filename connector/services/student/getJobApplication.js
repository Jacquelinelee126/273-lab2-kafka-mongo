const JobApplication = require('../../models/jobApplication');
const User = require('../../models/user');
const { STATUS_CODE, MESSAGES } = require('../../utils');
const mongoose = require('mongoose');

getJobApplicationService = async (msg, callback) => {
    const userID = msg.userID;
    const limit = parseInt(msg.limit);
    const page = parseInt(msg.page);

    console.log('limit =====> ', limit);
    console.log('page =====> ', page);

    let response = {};
    let error = {};
    try {
        const array = await JobApplication.find({
            applicant:mongoose.Types.ObjectId(userID)
        });

        const total = array.length;
        const applications = await JobApplication.aggregate([
            { $match: { applicant: mongoose.Types.ObjectId(userID) } },
            { $skip: (page - 1) * limit },
            { $limit: limit },
            {
                $lookup: {
                    from: 'jobs',
                    localField: 'job', //field from the input documents
                    foreignField: '_id', //field from the documents o
                    as: 'job', // output array field
                },
            },
            { $unwind: '$job' },
            {
                $lookup: {
                    from: 'basics',
                    localField: 'applicant',
                    foreignField: '_owner',
                    as: 'basic',
                },
            },
            // { $project: { resume: 0 } },
        ]);

        // const applications = await JobApplication.find();
        response.status = STATUS_CODE.SUCCESS;
        response.message = MESSAGES.SUCCESS;
        response.data = { total, applications };

        callback(null, response);
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.getJobApplicationService = getJobApplicationService;
