const Company = require('../../models/companyProfile');
const { STATUS_CODE, MESSAGES } = require('../../utils');
const mongoose = require('mongoose');

getOneCompanyService = async (msg, callback) => {
    const company = msg.company;

    let response = {};
    let error = {};

    try {
        result = await Company.findOne({
            name:company
        })
        response.status = STATUS_CODE.SUCCESS;
        response.message = MESSAGES.SUCCESS;
        response.data = result;
        callback(null, response);
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.getOneCompanyService = getOneCompanyService;
