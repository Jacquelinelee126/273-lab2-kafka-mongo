const Job = require('../../models/job');
const { STATUS_CODE, MESSAGES } = require('../../utils');
const mongoose = require('mongoose');

getOneJobService = async (msg, callback) => {
    const jID = msg.jID;

    let response = {};
    let error = {};

    try {
        resultArray = await Job.aggregate([
            {
                $match: {
                    _id: mongoose.Types.ObjectId(jID),
                },
            },
            // { $unwind: '$job' },
            // {
            //     $unwind: {
            //         path: '$profile.basics',
            //         preserveNullAndEmptyArrays: true,
            //     },
            // },
            {
                $lookup: {
                    from: 'companyprofiles',
                    localField: 'owner', //field from the input documents
                    foreignField: 'owner', //field from the documents o
                    as: 'company', // output array field
                },
            },
            { $unwind: '$company' },
            // { $match: { 'basic.name': { $regex: keyword, $options: 'i' } } },
            // { $project: {'basic.name': 1}},

            // { $project: {'education.major': 1}},
            // {
            //     $lookup: {
            //         from: 'experiences',
            //         localField: '_id', //field from the input documents
            //         foreignField: 'owner', //field from the documents of the "from" collection
            //         as: 'experience', // output array field
            //     },
            // },

            // {
            //     $project: { role: 0, email: 0, password: 0, createdAt: 0, updatedAt: 0, __v: 0 },
            // },
        ]);

        // console.log("resultArray.length====>", resultArray.length);
        let temp = {};
        if (resultArray.length != 0) {
            temp = resultArray[0];
            let { company } = temp;

            let job = {
                title: temp.title,
                location: temp.location,
                salary: temp.salary,
                jobDescription: temp.jobDescription,
                category: temp.category,
                applicationDeadline: temp.applicationDeadline
            };
            company = temp.company;
            // result = {...company, ...job}
            response.data = { company, job };
        } else {
            response.data = temp;
        }

        response.status = STATUS_CODE.SUCCESS;
        response.message = MESSAGES.SUCCESS;

        callback(null, response);
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.getOneJobService = getOneJobService;
