const Job = require('../../models/job');
const { STATUS_CODE, MESSAGES } = require('../../utils');

getAllJobsService = async (msg, callback) => {
    // const userID = msg.userID;
    console.log(msg);
    const limit = parseInt(msg.limit);
    const page = parseInt(msg.page);

    console.log('limit =====> ', limit);
    console.log('page =====> ', page);
    let response = {};
    let error = {};
    try {
        const total = await Job.estimatedDocumentCount();
        // const jobs = await Job.find()
        //     .skip((page - 1) * limit)
        //     .limit(limit).sort('createdAt');

        const jobs = await Job.aggregate([
            {
                $skip: (page - 1) * limit,
            },
            {
                $limit: limit,
            },         
            {
                $lookup: {
                    from: 'companyprofiles',
                    localField: 'owner', //field from the input documents
                    foreignField: 'owner', //field from the documents o
                    as: 'company', // output array field
                },
            },
            { $unwind: '$company' },

        ]);

        // if (resultArray.length != 0) {
        //     temp = resultArray[0];
        //     let { company } = temp;

        //     let job = {
        //         title: temp.title,
        //         location: temp.location,
        //         salary: temp.salary,
        //         description: temp.jobDescription,
        //         catogry: temp.catogry,
        //     };
        //     company = temp.company;
        //     // result = {...company, ...job}
        //     response.data = { company, job };
        // } else {
        //     response.data = temp;
        // }

        response.status = STATUS_CODE.SUCCESS;
        response.message = MESSAGES.SUCCESS;
        response.data = { total, jobs };
        callback(null, response);
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.getAllJobsService = getAllJobsService;
