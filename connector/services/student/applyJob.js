const JobApplication = require('../../models/jobApplication');
const User = require('../../models/user');
const mongoose = require('mongoose');

const { STATUS_CODE, MESSAGES } = require('../../utils');

applyJobService = async (msg, callback) => {
    const userID = msg.userID;
    const resume = msg.resume;
    const jobID = msg.jobID;

    let response = {};
    let error = {};

    try {
        const user = await User.findById(userID);

        const application = await JobApplication.findOne({
            applicant: mongoose.Types.ObjectId(userID),
            job: mongoose.Types.ObjectId(jobID)
        });

        if (application) {
            error.status = STATUS_CODE.BAD_REQUEST;
            error.message = MESSAGES.ACTION_NOT_COMPLETE;
            error.data = 'You have applied this job aready.';
            return callback(error, null);
        }

        console.log(' RESUME =====> ', resume);
        const result = new JobApplication({
            job: mongoose.Types.ObjectId(jobID),
            applicant: userID,
            resume: resume.data,
        });

        await result.save();

        response.status = STATUS_CODE.SUCCESS;
        response.message = MESSAGES.SUCCESS;
        response.data = result;

        callback(null, response);
        console.log('RESULT ====> ', result);
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.applyJobService = applyJobService;
