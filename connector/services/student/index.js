const { createStudentProfileService } = require('./createStudentProfile');
const { updateStudentProfileService } = require('./updateStudentProfile');
const { deleteStudentProfileService } = require('./deleteStudentProfile');

const { getOneStudentProfileService } = require('./getOneStudentProfile');
const { getOneJobService } = require('./getOneJob');
const { getOneCompanyService} = require('./getOneCompany');

const { createBasicService } = require('./createBasic');
const { getBasicService } = require('./getBasic');
const { updateBasicService } = require('./updateBasic');

const { createEducationService } = require('./createEducation');
const { getEducationService } = require('./getEducation');
const { updateEducationService } = require('./updateEducation');

const { createExperienceService } = require('./createExperience');
const { getExperienceService } = require('./getExperience');
const { getExperienceByIDService } = require('./getExperienceByID');
const { updateExperienceService } = require('./updateExperience');

const { applyJobService } = require('./applyJob');
const { getJobApplicationService } = require('./getJobApplication');
const { registerEventService } = require('./resgisterEvent');

const { searchJobsService } = require('./searchJobs');
const { searchEventsService } = require('./searchEvents');
const { searchStudentsService } = require('./searchStudents');

const { getAllEventsService } = require('./getAllEvents');
const { getAllJobsService } = require('./getAllJobs');
const { getAllStudentsService } = require('./getAllStudents');

let handle_request = (msg, callback) => {
    switch (msg.task) {
        case 'applyJob':
            applyJobService(msg, callback);
            break;
        case 'getJobApplication':
            getJobApplicationService(msg, callback);
            break;
        case 'registerEvent':
            registerEventService(msg, callback);
            break;
        case 'searchJobs':
            searchJobsService(msg, callback);
            break;
        case 'searchEvents':
            searchEventsService(msg, callback);
            break;
        case 'searchStudents':
            searchStudentsService(msg, callback);
            break;
        case 'createStudentProfile':
            createStudentProfileService(msg, callback);
            break;

        case 'getOneStudentProfile':
            getOneStudentProfileService(msg, callback);
            break;
        case 'getOneJob':
            getOneJobService(msg, callback);
            break;
        case 'getOneCompany':
            getOneCompanyService(msg, callback);
            break;

        case 'updateStudentProfile':
            updateStudentProfileService(msg, callback);
            break;
        case 'deleteStudentProfile':
            deleteStudentProfileService(msg, callback);
            break;
        case 'createBasic':
            createBasicService(msg, callback);
            break;
        case 'getBasic':
            getBasicService(msg, callback);
            break;
        case 'updateBasic':
            updateBasicService(msg, callback);
            break;
        case 'createEducation':
            createEducationService(msg, callback);
            break;
        case 'getEducation':
            getEducationService(msg, callback);
            break;
        case 'updateEducation':
            updateEducationService(msg, callback);
            break;
        case 'createExperience':
            createExperienceService(msg, callback);
            break;
        case 'getExperience':
            getExperienceService(msg, callback);
            break;
        case 'getExperienceByID':
            getExperienceByIDService(msg, callback);
            break;
        case 'updateExperience':
            updateExperienceService(msg, callback);
            break;
        case 'getAllEvents':
            getAllEventsService(msg, callback);
            break;
        case 'getAllJobs':
            getAllJobsService(msg, callback);
            break;
        case 'getAllStudents':
            getAllStudentsService(msg, callback);
            break;
        default:
            console.error('UNRECOGNIZED TASK', msg.task);
    }
};

module.exports = handle_request;
