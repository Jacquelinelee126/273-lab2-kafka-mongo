const Experience = require('../../models/experience');
const { STATUS_CODE, MESSAGES } = require('../../utils');
const mongoose = require('mongoose');

createExperienceService = async (msg, callback) => {
    const userID = msg.userID;

    let response = {};
    let error = {};
    try {
        exp = new Experience({
            ...msg,
            owner: userID,
        });

        await exp.save();

        response.status = STATUS_CODE.CREATE_SUCCESSFULLY;
        response.message = MESSAGES.CREATE_SUCCESSFULLY;
        response.data = exp;

        return callback(null, response);
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;

        return callback(error, null);
    }
};

module.exports.createExperienceService = createExperienceService;
