const Event = require('../../models/event');
const { STATUS_CODE, MESSAGES } = require('../../utils');

searchEventsService = async (msg, callback) => {
    // const userID = msg.userID;
    console.log(msg);
    const title = msg.title;
    const limit = parseInt(msg.limit);
    const page = parseInt(msg.page);

    let response = {};
    let error = {};
    try {
        const total = await Event.find({ title: new RegExp(title, 'i') }).countDocuments();
        const events = await Event.find({ title: new RegExp(title, 'i') })
            .sort('-time')
            .skip(page - 1)
            .limit(limit);

        response.status = STATUS_CODE.SUCCESS;
        response.message = MESSAGES.SUCCESS;
        response.data = { total, events };
        callback(null, response);
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.searchEventsService = searchEventsService;

// await req.user.populate({
//     path: 'tasks',
//     match,
//     options: {
//         limit: parseInt(req.query.limit),
//         skip: parseInt(req.query.skip),
//         sort
//     }
// }).execPopulate()
// res.send(req.user.tasks)
