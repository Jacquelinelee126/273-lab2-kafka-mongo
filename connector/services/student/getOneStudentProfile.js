// const StudentProfile = require('../../models/studentProfile');
// const Basic = require('../../models/basic');
// const Education = require('../../models/education');
// const Experience = require('../../models/experience');
const User = require('../../models/user');
const { STATUS_CODE, MESSAGES } = require('../../utils');
const mongoose = require('mongoose');

getOneStudentProfileService = async (msg, callback) => {
    const sID = msg.sID;

    let response = {};
    let error = {};

    try {
        resultArray = await User.aggregate([
            {
                $match: {
                    _id: mongoose.Types.ObjectId(sID),
                },
            },
            // { $unwind: '$profile' },
            // {
            //     $unwind: {
            //         path: '$profile.basics',
            //         preserveNullAndEmptyArrays: true,
            //     },
            // },          
            {
                $lookup: {
                    from: 'basics',
                    localField: '_id', //field from the input documents
                    foreignField: 'owner', //field from the documents o
                    as: 'basic', // output array field
                },
            },
            { $unwind: '$basic' },
            // { $match: { 'basic.name': { $regex: keyword, $options: 'i' } } },
            // { $project: {'basic.name': 1}},
            {
                $lookup: {
                    from: 'educations',
                    localField: '_id', //field from the input documents
                    foreignField: 'owner', //field from the documents of the "from" collection
                    as: 'education', // output array field
                },
            },
            { $unwind: '$education' },
            // { $project: {'education.major': 1}},
            {
                $lookup: {
                    from: 'experiences',
                    localField: '_id', //field from the input documents
                    foreignField: 'owner', //field from the documents of the "from" collection
                    as: 'experience', // output array field
                },
            },

            // {
            //     $project: { role: 0, email: 0, password: 0, createdAt: 0, updatedAt: 0, __v: 0 },
            // },
        ]);

        console.log("resultArray.length====>", resultArray.length);
        result = {}
        if(resultArray.length != 0){
            result = resultArray[0];
        }
        console.log('result ====> ', result);
        response.status = STATUS_CODE.SUCCESS;
        response.message = MESSAGES.SUCCESS;
        response.data = result;
        callback(null, response);
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.getOneStudentProfileService = getOneStudentProfileService;
