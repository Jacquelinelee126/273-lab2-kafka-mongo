const EventRegistration = require('../../models/eventRegistration');
const Education = require('../../models/education');
const Event = require('../../models/event');
const mongoose = require('mongoose');
const { STATUS_CODE, MESSAGES } = require('../../utils');

registerEventService = async (msg, callback) => {
    const userID = msg.userID;
    const eventID = msg.eventID;

    let response = {};
    let error = {};
    try {
        const registration = await EventRegistration.findOne({
            applicant: userID,
            event: eventID,
        });

        if (registration) {
            error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
            error.message = MESSAGES.ACTION_NOT_COMPLETE;
            error.data = 'You have regietered this event already!';
            return callback(error, null);
        }
        const education = await Education.findOne({ owner: userID });
        const event = await Event.findById(eventID);

        // console.log('education =====>', education.major);
        // console.log('event=====>', event.major);

        if (event.major == 'All' || (education && education.major == event.major)) {
            const result = new EventRegistration({
                event: mongoose.Types.ObjectId(eventID),
                applicant: userID,
            });

            await result.save();

            response.status = STATUS_CODE.SUCCESS;
            response.message = MESSAGES.SUCCESS;
            response.data = result;

            callback(null, response);
        } else {
            if (!education) {
                error.data = 'Please upload your education profile and verify your major';
            }

            if (education.major != event.major) {
                error.data = 'Sorry. Your major is not allowed for this event.';
            }
            error.status = STATUS_CODE.BAD_REQUEST;
            error.message = MESSAGES.ACTION_NOT_COMPLETE;
            return callback(error, null);
        }
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        return callback(error, null);
    }
};

module.exports.registerEventService = registerEventService;
