const Experience = require('../../models/experience');
const { STATUS_CODE, MESSAGES } = require('../../utils');
const mongoose = require('mongoose');

updateExperienceService = async (msg, callback) => {
    const userID = msg.userID;
    const expID = msg.expID;
    const body = msg.body;

    let response = {};
    let error = {};

    console.log("expID", expID);

    try {
        let result = await Experience.findById(expID);

        if (!result) {
            error.status = STATUS_CODE.BAD_REQUEST;
            error.message = MESSAGES.DATA_NOT_FOUND;
            error.data = result;
            return callback(error, null);
        } else {
            // console.log('RESULT before UPDATE =======> ', result);
            temp = { ...result._doc };
            // console.log('temp =======> ', temp);
            temp = { ...body };
            // console.log('temp after body=======> ', temp);
            const exp = new Experience({
                owner: mongoose.Types.ObjectId(userID),
                _id: result._id,
                ...temp,
            });
            await Experience.deleteOne(result);
            await exp.save();
            
            response.status = STATUS_CODE.SUCCESS;
            response.message = MESSAGES.SUCCESS;
            response.data = exp;

            return callback(null, response);
        }
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.updateExperienceService = updateExperienceService;
