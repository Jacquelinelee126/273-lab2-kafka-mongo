const Education = require('../../models/education');
const { STATUS_CODE, MESSAGES } = require('../../utils');
const mongoose = require('mongoose');

createEducationService = async (msg, callback) => {
    const userID = msg.userID;

    let response = {};
    let error = {};
    try {
        const education = await Education.findOne({ owner: mongoose.Types.ObjectId(userID) });
        console.log("profile found ? ===> ", Education);

        if (!education) {
            edu = new Education({
                ...msg,
                owner: userID,
            });

            await edu.save();

            response.status = STATUS_CODE.CREATE_SUCCESSFULLY;
            response.message = MESSAGES.CREATE_SUCCESSFULLY;
            response.data = edu;

            return callback(null, response);
        } else {
            error.status = STATUS_CODE.BAD_REQUEST;
            error.message = MESSAGES.DATA_ALREADY_EXIST;
            return callback(error, null);
        }
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;

        return callback(error, null);
    }
};

module.exports.createEducationService = createEducationService;
