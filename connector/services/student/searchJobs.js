const CompanyProfile = require('../../models/companyProfile');
const User = require('../../models/user');
const Job = require('../../models/job');
const { STATUS_CODE, MESSAGES } = require('../../utils');

searchJobsService = async (msg, callback) => {
    // const userID = msg.userID;
    console.log(msg);
    const keyword = msg.keyword;
    const jobCategory = msg.jobCategory;
    const jobLocation = msg.jobLocation;

    let response = {};
    let error = {};
    try {
        const companyArr = await CompanyProfile.find({ name: new RegExp(keyword, "i")});
        const companyIDArr = companyArr.map((company) => company._id);

        jobArray = await Promise.all(
            companyIDArr.map(async (cID) => {
                company = await CompanyProfile.findById(cID);
                user = await User.findById(company.owner);
                return await Job.find({ owner: user._id });
            }),
        );

        console.log();
        jobArray2 = await Job.find({title: new RegExp(keyword, "i")});

        flatJobArray = jobArray.flat().concat(jobArray2);

        result = flatJobArray
            .filter((e) => (jobCategory ? e.category == jobCategory : true))
            .filter((e) => (jobLocation ? e.location === jobLocation : true));

        response.status = STATUS_CODE.SUCCESS;
        response.message = MESSAGES.SUCCESS;
        response.data = result;
        callback(null, response);
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.searchJobsService = searchJobsService;

// await req.user.populate({
//     path: 'tasks',
//     match,
//     options: {
//         limit: parseInt(req.query.limit),
//         skip: parseInt(req.query.skip),
//         sort
//     }
// }).execPopulate()
// res.send(req.user.tasks)
