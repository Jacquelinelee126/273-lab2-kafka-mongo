const Experience = require('../../models/experience');
const { STATUS_CODE, MESSAGES } = require('../../utils');

getExperienceByIDService = async (msg, callback) => {
    const expID = msg.expID;

    let response = {};
    let error = {};

    try {
        const result = await Experience.findById(expID);

        console.log("result ? ====>", result);
        if (!result) {
            error.status = STATUS_CODE.NOT_FOUND;
            error.message = MESSAGES.DATA_NOT_FOUND;

            return callback(error, null);
        } else {
            response.status = STATUS_CODE.SUCCESS;
            response.message = MESSAGES.SUCCESS;
            response.data = result;

            return callback(null, response);
        }
    } catch (err) {
        error.status = STATUS_CODE.BAD_REQUEST;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.getExperienceByIDService = getExperienceByIDService;
