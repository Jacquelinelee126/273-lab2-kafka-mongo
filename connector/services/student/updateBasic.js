const Basic = require('../../models/basic');
const { STATUS_CODE, MESSAGES } = require('../../utils');
const mongoose = require('mongoose');

updateBasicService = async (msg, callback) => {
    const userID = msg.userID;
    const body = msg.body;

    let response = {};
    let error = {};

    try {
        let result = await Basic.findOne({ owner: mongoose.Types.ObjectId(userID) });

        if (!result) {
            error.status = STATUS_CODE.BAD_REQUEST;
            error.message = MESSAGES.DATA_NOT_FOUND;
            error.data = result;
            return callback(error, null);
        } else {
            // console.log('RESULT before UPDATE =======> ', result);
            temp = { ...result._doc };
            // console.log('temp =======> ', temp);
            temp = { ...body };
            // console.log('temp after body=======> ', temp);
            const basic = new Basic({ owner: mongoose.Types.ObjectId(userID), ...temp });
            await Basic.deleteOne(result);
            await basic.save();
            console.log('body IN UPDATE =======> ', body);

            console.log('RESULT IN UPDATE =======> ', result);

            response.status = STATUS_CODE.SUCCESS;
            response.message = MESSAGES.SUCCESS;
            response.data = basic;

            return callback(null, response);
        }
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.updateBasicService = updateBasicService;
