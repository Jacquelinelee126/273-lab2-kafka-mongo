const User = require('../../models/user');

const { STATUS_CODE, MESSAGES } = require('../../utils');

// 1. View list of students enrolled in Handshake
// 2. Search for students (using student name or college name)
// 3. Filter the results based on their Major
// 4. Pagination should be implemented.
// 5. Click on Student details to view Student Profile

// basic name ---> user id ---> major
searchStudentsService = async (msg, callback) => {
    // const userID = msg.userID;
    console.log(msg);
    const keyword = msg.keyword;
    const major = msg.major;
    console.log('MAJOR: ', major);
    let response = {};
    let error = {};

    try {
        result = await User.aggregate([
            { $match: { role: 'student' } },
            {
                $lookup: {
                    from: 'basics',
                    localField: '_id', //field from the input documents
                    foreignField: 'owner', //field from the documents o
                    as: 'basic', // output array field
                },
            },
            { $unwind: '$basic' },
            { $match: { 'basic.name': { $regex: new RegExp(keyword, "i") } } },
            // { $project: {'basic.name': 1}},
            {
                $lookup: {
                    from: 'educations',
                    localField: '_id', //field from the input documents
                    foreignField: 'owner', //field from the documents of the "from" collection
                    as: 'education', // output array field
                },
            },
            { $unwind: '$education' },
            { $match: { 'education.major': { $regex:new RegExp(major, "i")} } },
            // { $project: {'education.major': 1}},
            {
                $project: { role: 0, email: 0, password: 0, createdAt: 0, updatedAt: 0, __v: 0 },
            },
        ]);

        response.status = STATUS_CODE.SUCCESS;
        response.message = MESSAGES.SUCCESS;
        response.data = result;
        callback(null, response);
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.searchStudentsService = searchStudentsService;

// try {
//   const studentArr = await Basic.find({ name: new RegExp(keyword, "i")});
//   const companyIDArr = companyArr.map((company) => company._id);

//   jobArray = await Promise.all(
//       companyIDArr.map(async (cID) => {
//           company = await CompanyProfile.findById(cID);
//           user = await User.findById(company.owner);
//           return await Job.find({ owner: user._id });
//       }),
//   );

//   console.log();
//   jobArray2 = await Job.find({title: new RegExp(keyword, "i")});

//   flatJobArray = jobArray.flat().concat(jobArray2);

//   result = flatJobArray
//       .filter((e) => (major ? e.major == major : true));


// {
//     $lookup: {
//         from: 'basics',
//         localField: '_id', //field from the input documents
//         foreignField: 'owner', //field from the documents o
//         // let: { id: '$_id' },
//         // pipeline: [
//         //     {
//         //         $match: {
//         //             $expr: {
//         //                 $and: [
//         //                     { $eq: ['$$id', '$owner'] },
//         //                     //{ $regexMatch: { input: '$name' , regex:  keyword, options: 'i' } },
//         //                 ],
//         //             },
//         //         },
//         //     },
//         //     { $project: { name: 1 } },
//         // ],
//         as: 'basic', // output array field
//     },
// },
