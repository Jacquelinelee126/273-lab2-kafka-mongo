const StudentProfile = require('../../models/studentProfile');
const { STATUS_CODE, MESSAGES } = require('../../utils');
const mongoose = require('mongoose');

deleteStudentProfileService = async (msg, callback) => {
  const userID = msg.userID;
  console.log("TYPE OF USER ID", typeof userID);
  let response = {};
  let error = {};

  try{
    const temp = await StudentProfile.findOne({owner: mongoose.Types.ObjectId(userID)});
    console.log("temp ======> ", temp);

    await StudentProfile.findOneAndDelete({owner: mongoose.Types.ObjectId(userID)});
    response.status = STATUS_CODE.SUCCESS;
    response.message = MESSAGES.DELETE_SUCCESSFULLY;

    callback(null, response);
  }  catch (err) {
    error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
    error.message = MESSAGES.ACTION_NOT_COMPLETE;
    error.data = err;

    callback(error, null);
  }
};

module.exports.deleteStudentProfileService = deleteStudentProfileService;
