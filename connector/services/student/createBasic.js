const Basic = require('../../models/basic');
const { STATUS_CODE, MESSAGES } = require('../../utils');
const mongoose = require('mongoose');

createBasicService = async (msg, callback) => {
    const userID = msg.userID;
    console.log("msg====>", msg);

    let response = {};
    let error = {};
    try {
        const basic = await Basic.findOne({ owner: mongoose.Types.ObjectId(userID) });
        console.log("profile found ? ===> ", basic);

        if (!basic) {
            bp = new Basic({
                ...msg,
                owner: userID,
            });

            await bp.save();

            response.status = STATUS_CODE.CREATE_SUCCESSFULLY;
            response.message = MESSAGES.CREATE_SUCCESSFULLY;
            response.data = bp;

            return callback(null, response);
        } else {
            error.status = STATUS_CODE.BAD_REQUEST;
            error.message = MESSAGES.DATA_ALREADY_EXIST;
            return callback(error, null);
        }
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;

        return callback(error, null);
    }
};

module.exports.createBasicService = createBasicService;
