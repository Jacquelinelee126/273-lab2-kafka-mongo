const Event = require('../../models/event');
const { STATUS_CODE, MESSAGES } = require('../../utils');

getAllEventsService = async (msg, callback) => {
    const limit = parseInt(msg.limit);
    const page = parseInt(msg.page);

    // console.log('limit', limit);
    // console.log('page', page);
    let response = {};
    let error = {};
    try {
        const total = await Event.find().estimatedDocumentCount();
        const events = await Event.find().sort('-time').skip((page - 1) * limit).limit(limit);

        response.status = STATUS_CODE.SUCCESS;
        response.message = MESSAGES.SUCCESS;
        response.data = { total, events };
        callback(null, response);
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.getAllEventsService = getAllEventsService;
