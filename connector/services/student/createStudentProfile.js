const StudentProfile = require('../../models/studentProfile');
const { STATUS_CODE, MESSAGES } = require('../../utils');
const mongoose = require('mongoose');

createStudentProfileService = async (msg, callback) => {
    const userID = msg.userID;

    let response = {};
    let error = {};
    try {
        const profile = await StudentProfile.findOne({ owner: mongoose.Types.ObjectId(userID) });
        console.log("profile found ? ===> ", profile);

        if (!profile) {
            sp = new StudentProfile({
                ...msg,
                owner: userID,
            });

            await sp.save();

            response.status = STATUS_CODE.CREATE_SUCCESSFULLY;
            response.message = MESSAGES.CREATE_SUCCESSFULLY;
            response.data = sp;

            return callback(null, response);
        } else {
            error.status = STATUS_CODE.BAD_REQUEST;
            error.message = MESSAGES.DATA_ALREADY_EXIST;
            return callback(error, null);
        }
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;

        return callback(error, null);
    }
};

module.exports.createStudentProfileService = createStudentProfileService;
