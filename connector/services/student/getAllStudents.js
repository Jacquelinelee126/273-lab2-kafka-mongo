// const Basic = require('../../models/basic');
const User = require('../../models/user');
// const Education = require('../../models/education');

const { STATUS_CODE, MESSAGES } = require('../../utils');

getAllStudentsService = async (msg, callback) => {
    // const userID = msg.userID;
    // console.log(msg);
    const limit = parseInt(msg.limit);
    const page = parseInt(msg.page);
    // console.log('limit', limit);
    // console.log('page', page);

    let response = {};
    let error = {};
    try {
        students = await User.aggregate([
            { $match: { role: 'student' } },
            { $skip: (page - 1) * limit },
            { $limit: limit },
            {
                $lookup: {
                    from: 'basics',
                    localField: '_id', //field from the input documents
                    foreignField: 'owner', //field from the documents o
                    // let: { name: "$name", city: "$city"},
                    // pipeline:  [{$project: {name: 1}}],
                    as: 'basic', // output array field
                },
            },
            // { $unwind: '$basic' },
            {
                $lookup: {
                    from: 'educations',
                    localField: '_id', //field from the input documents
                    foreignField: 'owner', //field from the documents of the "from" collection
                    as: 'education', // output array field
                },
            },
            // { $unwind: '$education' },
            {
                $lookup: {
                    from: 'experiences',
                    localField: '_id',
                    foreignField: 'owner',
                    as: 'experiences',
                },
            },
            {
                $project: { role: 0, email: 0, password: 0, createdAt: 0, updatedAt: 0, __v: 0 },
            },
        ]);

        const total = students.length;
        response.status = STATUS_CODE.SUCCESS;
        response.message = MESSAGES.SUCCESS;
        response.data = { total, students };
        callback(null, response);
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.getAllStudentsService = getAllStudentsService;
