const User = require('../../models/user');
const mongoose = require('mongoose');

uploadResumeService = async (msg, callback) => {
  console.log('Inside book kafka backend - message from passed payload', msg);
  const resume = msg.resume;
  const userID = msg.userID;

  try {
    user = await User.findById({
      _id: mongoose.Types.ObjectId(userID)
    });

    user.resume = resume;

    console.log("Equal====>", user.avatar == user.resume)

    user.save();

    callback(null, user);
  } catch (error) {
    callback(error, null);
  }
};

module.exports.uploadResumeService = uploadResumeService;
