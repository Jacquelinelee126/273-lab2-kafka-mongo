const { loginService } = require('./login');
const { registrationService } = require('./registration');
const { authenticationService } = require('./authentication');
const { uploadAvatarService } = require('./uploadAvatar');
const { getAvatarService } = require('./getAvatar');
const { uploadResumeService } = require('./uploadResume');

let handle_request = (msg, callback) => {
    switch (msg.task) {
        case 'authentication':
            authenticationService(msg, callback);
            break;
        case 'registration':
            registrationService(msg, callback);
            break;
        case 'login':
            loginService(msg, callback);
            break;
        case 'uploadAvatar':
            uploadAvatarService(msg, callback);
            break;
        case 'getAvatar':
            getAvatarService(msg, callback);
            break;

            case 'uploadResume':
              uploadResumeService(msg, callback);
              break;
    }
};

module.exports = handle_request;
