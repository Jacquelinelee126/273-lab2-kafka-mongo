const User = require('../../models/user');
const { STATUS_CODE, MESSAGES } = require('../../utils');

loginService = async (msg, callback) => {
    console.log('Inside book kafka backend - post');
    console.log('Inside book kafka backend - message from passed payload', msg);
    let response = {};
    let error = {};

    try {
        const user = await User.findByCredentials(msg.email, msg.password);
        // console.log("USER", user);
        const token = await user.generateAuthToken();

        response.status = STATUS_CODE.SUCCESS;
        response.message = MESSAGES.SUCCESS;
        response.data = token;

        callback(null, response);

    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = "Unable to login. Invalid credentials";
        callback(error, null);
    }
};

module.exports.loginService = loginService;
