const User = require('../../models/user');
const mongoose = require('mongoose');

uploadAvatarService = async (msg, callback) => {
  console.log('Inside book kafka backend - post');
  console.log('Inside book kafka backend - message from passed payload', msg);
  const avatar = msg.avatar;
  const userID = msg.userID;

  try {
    user = await User.findById({
      _id: mongoose.Types.ObjectId(userID)
    });

    user.avatar = avatar;

    user.save();
    callback(null, user);
  } catch (error) {
    callback(error, null);
  }
};

module.exports.uploadAvatarService = uploadAvatarService;
