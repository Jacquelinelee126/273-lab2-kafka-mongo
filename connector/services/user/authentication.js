const User = require('../../models/user');

authenticationService = async (msg, callback) => {
  console.log('Inside book kafka backend - authetication');
  console.log('Inside book kafka backend - message from passed payload', msg);

  const userID = msg.userID;
  try {
    const user = await User.findOne({_id: userID});

    if (user) {
      callback(null, user);
    } else {
      callback(null, false);
    }
  } catch (error) {
    callback(error, false);
  }
};

module.exports.authenticationService = authenticationService;



