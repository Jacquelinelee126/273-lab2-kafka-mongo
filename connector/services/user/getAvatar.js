const User = require('../../models/user');
const mongoose = require('mongoose');

getAvatarService = async (msg, callback) => {
  console.log('Inside book kafka backend - message from passed payload', msg);
  const userID = msg.userID;

  try {
    const user = await User.findById({
      _id: mongoose.Types.ObjectId(userID)
    });

    if(!user || !user.avatar){
      callback(new Error(), null)
    } else {
      result = user.avatar;
      callback(null, result);
    }
  } catch (error) {
    callback(error, null);
  }
};

module.exports.getAvatarService = getAvatarService;