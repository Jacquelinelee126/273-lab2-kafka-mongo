const User = require('../../models/user');
const { STATUS_CODE, MESSAGES } = require('../../utils');

registrationService = async (msg, callback) => {
    console.log('Inside book kafka backend - post');
    console.log('Inside book kafka backend - message from passed payload', msg);

    const username = msg.username;
    const email = msg.email;
    const role = msg.role;
    const password = msg.password;
    let response = {};
    let error = {};

    try {
        const test = await User.find({
            email: msg.username,
        });

        if(test) {
            error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
            error.message = MESSAGES.ACTION_NOT_COMPLETE;
            error.data = "This email has been registered already!";
            callback(error, null);
        }

        const user = new User({
            username,
            email,
            role,
            password,
        });
        await user.save();
        response.status = STATUS_CODE.SUCCESS;
        response.message = MESSAGES.SUCCESS;
        response.data = 'Registration is successful!';

        callback(null, response);
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.registrationService = registrationService;
