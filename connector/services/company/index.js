const { getAllEventsService } = require('./getAllEvents');
const { getAllJobsService } = require('./getAllJobs');
const { getAllRegistrationForOneEventService } = require('./getAllRegistrationForOneEvent');
const { getOneApplicationService } = require('./getOneApplication');
const { updateJobApplicationStatusService } = require('./updateJobApplicationStatus');
const { getCompanyProfileService } = require('./getCompanyProfile');
const { createCompanyProfileService } = require('./createCompanyProfile');
const { updateCompanyProfileService } = require('./updateCompanyProfile');
const { deleteCompanyProfileService } = require('./deleteCompanyProfile');
const { getAllApplicationsForOneJobService } = require('./getAllApplicationsForOneJob');
// const { searchStudentsService } = require('./searchStudents');

let handle_request = (msg, callback) => {
    switch (msg.task) {
        case 'getAllEvents':
            getAllEventsService(msg, callback);
            break;
            
        case 'getAllJobs':
            getAllJobsService(msg, callback);
            break;
        case 'getAllRegistrationForOneEvent':
            getAllRegistrationForOneEventService(msg, callback);
            break;
        case 'getAllApplicationsForOneJob':
            getAllApplicationsForOneJobService(msg, callback);
            break;
        case 'getOneApplication':
            getOneApplicationService(msg, callback);
            break;
        case 'updateJobApplicationStatus':
            updateJobApplicationStatusService(msg, callback);
            break;
        case 'getCompanyProfile':
            getCompanyProfileService(msg, callback);
            break;
        case 'createCompanyProfile':
            createCompanyProfileService(msg, callback);
            break;
        case 'updateCompanyProfile':
            updateCompanyProfileService(msg, callback);
            break;
        case 'deleteCompanyProfile':
            deleteCompanyProfileService(msg, callback);
            break;
        // case 'searchStudents':
        //     searchStudentsService(msg, callback);
        //     break;
        default:
            console.error('UNRECOGNIZED TASK',msg.task);
    }
};

module.exports = handle_request;
