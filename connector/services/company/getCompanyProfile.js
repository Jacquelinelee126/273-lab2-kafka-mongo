const CompanyProfile = require('../../models/companyProfile');
const { STATUS_CODE, MESSAGES } = require('../../utils');
const mongoose = require('mongoose');

getCompanyProfileService = async (msg, callback) => {
    console.log("INSIDE SERVICE IMPLEMENTATION ====> ");
    const userID = msg.userID;
    // const role = msg.role;

    let response = {};
    let error = {};

    try {
        let result = await CompanyProfile.findOne({ owner: mongoose.Types.ObjectId(userID) });
        console.log("result ? ====>", result);

        if (!result) {
            error.status = STATUS_CODE.NOT_FOUND;
            error.message = MESSAGES.DATA_NOT_FOUND;

            return callback(error, null);
        } else {
            response.status = STATUS_CODE.SUCCESS;
            response.message = MESSAGES.SUCCESS;
            response.data = result;

            return callback(null, response);
        }
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.getCompanyProfileService = getCompanyProfileService;
