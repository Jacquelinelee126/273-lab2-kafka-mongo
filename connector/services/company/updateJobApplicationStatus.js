const JobApplication = require('../../models/jobApplication');
const { STATUS_CODE, MESSAGES } = require('../../utils');

updateJobApplicationStatusService = async (msg, callback) => {
    // const userID = msg.userID;
    const applicationID = msg.applicationID;
    const status = msg.status;

    let response = {};
    let error = {};
    try {
        // console.log('status ====>', status);
        app = await JobApplication.findById(applicationID);
        // console.log("app===>", app);
        app.status = status;

        await app.save();

        response.status = STATUS_CODE.SUCCESS;
        response.message = MESSAGES.SUCCESS;
        response.data = app;

        callback(null, response);
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err.message;
        callback(error, null);
    }
};

module.exports.updateJobApplicationStatusService = updateJobApplicationStatusService;
