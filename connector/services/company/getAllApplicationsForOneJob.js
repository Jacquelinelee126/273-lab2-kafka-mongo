const JobApplication = require('../../models/jobApplication');
const { STATUS_CODE, MESSAGES } = require('../../utils');
const mongoose  = require('mongoose');
getAllApplicationsForOneJobService = async (msg, callback) => {
    // const userID = msg.userID;
    const jobID = msg.jobID;
    let response = {};
    let error = {};

    try {
        const total = await JobApplication.find({ job: jobID });
        const applications = await JobApplication.aggregate([
            {
                $match: {job: mongoose.Types.ObjectId(jobID)}
            },
            {
                $lookup: {
                    from: 'basics',
                    localField: 'applicant', //field from the input documents
                    foreignField: 'owner', //field from the documents o
                    as: 'student',
                }
            },
            { $unwind: '$student' }      
        ])

        // resumeData = resume.data;

        response.status = STATUS_CODE.SUCCESS;
        response.message = MESSAGES.SUCCESS;
        response.data = applications

        callback(null, response);
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.getAllApplicationsForOneJobService = getAllApplicationsForOneJobService;
