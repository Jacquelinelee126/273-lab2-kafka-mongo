const { STATUS_CODE, MESSAGES } = require('../../utils');
const Job = require('../../models/job');
const mongoose = require('mongoose');

getAllJobsService = async (msg, callback) => {
    const userID = msg.userID;

    let response = {};
    let error = {};

    try {
        let result = await Job.find({ owner: mongoose.Types.ObjectId(userID) });
        response.status = STATUS_CODE.SUCCESS;
        response.message = MESSAGES.SUCCESS;
        response.data = result;

        callback(null, response);
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.getAllJobsService = getAllJobsService;
