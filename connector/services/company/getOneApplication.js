const JobApplication = require('../../models/jobApplication');
const mongoose = require('mongoose');

getOneApplicationService = async (msg, callback) => {
    // const userID = msg.userID;
    const jobID = msg.jobID;
    const applicationID = msg.applicationID;

    try {
        result = await JobApplication.find({ _id: mongoose.Types.ObjectId(applicationID) });

        console.log('RESULT ====> ', result);
        callback(null, result);
    } catch (error) {
        callback(error, null);
    }
};

module.exports.getOneApplicationService = getOneApplicationService;
