const CompanyProfile = require('../../models/companyProfile');
const { STATUS_CODE, MESSAGES } = require('../../utils');
const mongoose = require('mongoose');

updateCompanyProfileService = async (msg, callback) => {
    const userID = msg.userID;
    const body = msg.body;

    console.log('BODY=====>', body);

    let response = {};
    let error = {};

    try {
        let result = await CompanyProfile.findOne({ owner: mongoose.Types.ObjectId(userID) });

        console.log('result====>', result);
        if (!result) {
            error.status = STATUS_CODE.BAD_REQUEST;
            error.message = MESSAGES.DATA_NOT_FOUND;
            error.data = result;

            return callback(error, null);
        } else {         
            const _id = result._id;

            await CompanyProfile.findByIdAndDelete(_id);
            console.log("Temp after update====>", );
            const profile = new CompanyProfile({
                owner: mongoose.Types.ObjectId(userID),
                _id,
                ...body,
            });
            // console.log("PROFILE ===>",profile)
           
            await profile.save();
            response.status = STATUS_CODE.SUCCESS;
            response.message = MESSAGES.SUCCESS;
            response.data = profile;

            return callback(null, response);
        }
    } catch (err) {
        error.status = STATUS_CODE.BAD_REQUEST;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.updateCompanyProfileService = updateCompanyProfileService;
