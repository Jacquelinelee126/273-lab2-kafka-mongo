const StudentProfile = require('../../models/studentProfile');
const User = require('../../models/user');
const Job = require('../../models/job');
const { STATUS_CODE, MESSAGES } = require('../../utils');

searchStudentsService = async (msg, callback) => {
    // const userID = msg.userID;
    console.log(msg);
    const keyword = msg.keyword;

    let response = {};
    let error = {};
    try {
        // response.data = await StudentProfile.find().where({ name: new RegExp(keyword) });
        response.data = await StudentProfile.where({name: new RegExp(keyword)});

        response.status = STATUS_CODE.SUCCESS;
        response.message = MESSAGES.SUCCESS;
        callback(null, response);
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.searchStudentsService = searchStudentsService;

// await req.user.populate({
//     path: 'tasks',
//     match,
//     options: {
//         limit: parseInt(req.query.limit),
//         skip: parseInt(req.query.skip),
//         sort
//     }
// }).execPopulate()
// res.send(req.user.tasks)
