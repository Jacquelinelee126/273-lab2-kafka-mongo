const EventRegistration = require('../../models/eventRegistration');
const { STATUS_CODE, MESSAGES } = require('../../utils');
const mongoose = require('mongoose');

getAllRegistrationForOneEventService = async (msg, callback) => {
    // const userID = msg.userID;
    const eventID = msg.eventID;

    let response = {};
    let error = {};

    try {
        const result = await EventRegistration.aggregate([
            {
                $match: { event: mongoose.Types.ObjectId(eventID) },
            },
            {
                $lookup: {
                    from: 'basics',
                    localField: 'applicant',
                    foreignField: 'owner',
                    as: 'basic',
                },
            },

            { $unwind: '$basic' },
            // { $project: { 'basic.name': 1 } },
            {
                $lookup: {
                    from: 'educations',
                    localField: 'applicant',
                    foreignField: 'owner',
                    as: 'education',
                },
            },
            { $unwind: '$education' },
            // { $project: { 'education.collegaName': 1, 'education.major': 1 } },
        ]);

        // const result = await EventRegistration.find({
        //     event: eventID
        // });
        response.status = STATUS_CODE.SUCCESS;
        response.message = MESSAGES.SUCCESS;
        response.data = result;

        callback(null, response);
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.getAllRegistrationForOneEventService = getAllRegistrationForOneEventService;
