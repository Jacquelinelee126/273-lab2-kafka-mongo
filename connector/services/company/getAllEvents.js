const Event = require('../../models/event');
const { STATUS_CODE, MESSAGES } = require('../../utils');const mongoose = require('mongoose');

getAllEventsService = async (msg, callback) => {
    const userID = msg.userID;

    let response = {};
    let error = {};

    try {
        let result = await Event.find({ owner: mongoose.Types.ObjectId(userID) });
        response.status = STATUS_CODE.SUCCESS;
        response.message = MESSAGES.SUCCESS;
        response.data = result;

        callback(null, response);
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.getAllEventsService = getAllEventsService;
