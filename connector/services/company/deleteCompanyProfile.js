const CompanyProfile = require('../../models/companyProfile');
const { STATUS_CODE, MESSAGES } = require('../../utils');
const mongoose = require('mongoose');

deleteCompanyProfileService = async (msg, callback) => {
  const userID = msg.userID;
  console.log("TYPE OF USER ID", typeof userID);
  let response = {};
  let error = {};

  try{
    await CompanyProfile.findOneAndDelete({owner: mongoose.Types.ObjectId(userID)});
    response.status = STATUS_CODE.SUCCESS;
    response.message = MESSAGES.DELETE_SUCCESSFULLY;

    callback(null, response);
  }  catch (err) {
    error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
    error.message = MESSAGES.ACTION_NOT_COMPLETE;
    error.data = err;

    callback(error, null);
  }
};

module.exports.deleteCompanyProfileService = deleteCompanyProfileService;
