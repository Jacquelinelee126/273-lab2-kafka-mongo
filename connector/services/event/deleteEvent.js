const { STATUS_CODE, MESSAGES } = require('../../utils');
const Event = require('../../models/event');const mongoose = require('mongoose');

deleteEventService = async (msg, callback) => {
    const eventID = msg.eventID;

    console.log('Message Received=======>', msg);

    let response = {};
    let error = {};

    try {
        await Event.findByIdAndDelete(eventID);

        response.status = STATUS_CODE.SUCCESS;
        response.message = MESSAGES.DELETE_SUCCESSFULLY;

        callback(null, response);
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;

        callback(error, null);
    }
};

module.exports.deleteEventService = deleteEventService;
