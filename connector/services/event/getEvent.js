const { STATUS_CODE, MESSAGES } = require('../../utils');
const Event = require('../../models/event');const mongoose = require('mongoose');

getEventService = async (msg, callback) => {
  const eventID = msg.eventID;

  let response = {};
    let error = {};
    try {
        const event = await Event.findById(eventID);

        response.status = STATUS_CODE.SUCCESS;
        response.message = MESSAGES.SUCCESS;
        response.data = event;

        callback(null, response);
    } catch (err) {
      error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
      error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.getEventService = getEventService;
