const { getEventService } = require('./getEvent.js');
const { createEventService } = require('./creatEvent');
const { updateEventService } = require('./updateEvent');
const { deleteEventService } = require('./deleteEvent');

let handle_request = (msg, callback) => {
    switch (msg.task) {
        case 'getEventInfo':
            getEventService(msg, callback);
            break;
        case 'createEvent':
            createEventService(msg, callback);
            break;
        case 'updateEvent':
            updateEventService(msg, callback);
            break;
        case 'deleteEvent':
            deleteEventService(msg, callback);
            break;
        default:
            console.error('UNRECOGNIZED TASK', msg.task);
    }
};

module.exports = handle_request;
