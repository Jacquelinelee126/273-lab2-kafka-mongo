const { STATUS_CODE, MESSAGES } = require('../../utils');
const Event = require('../../models/event');

createEventService = async (msg, callback) => {
    const userID = msg.userID; // userID is just a string, need to convert it into objectID
    let response = {};
    let error = {};

    // console.log('MESSAGE RECEIVED ====> ', msg);
    try {
        const event = new Event({
            owner: userID,
            ...msg,
        });

        await event.save();
        response.status = STATUS_CODE.SUCCESS;
        response.message = MESSAGES.SUCCESS;
        response.data = event;

        callback(null, response);
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.createEventService = createEventService;
