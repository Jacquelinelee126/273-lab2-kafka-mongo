const { STATUS_CODE, MESSAGES } = require('../../utils');
const Event = require('../../models/event');const mongoose = require('mongoose');

updateEventService = async (msg, callback) => {

    const eventID = msg.eventID;
    let response = {};
    let error = {};

    let updates = [];
    let msgKeys = Object.keys(msg);
    console.log('msgKeys=====>', msgKeys);

    msgKeys.forEach((key) => {
        if (key !== 'task' && key !== 'userID' && key !== 'eventID') {
            updates.push(key);
        }
    });

    console.log('updates=====>', updates);
    const allowedUpdates = ['title', 'location', 'description', 'major', 'time'];

    const isValidOperation = updates.every((update) => allowedUpdates.includes(update));

    console.log(' isValidOperation===> ', isValidOperation);
    if (!isValidOperation) {
        error.status = STATUS_CODE.BAD_REQUEST;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = { err: 'Invalid updates!' };
        return callback(error, null);    }

        try {
            let event = await Event.findById(eventID);
            updates.forEach((update) => (event[update] = msg[update]));
            await event.save();
    
            response.status = STATUS_CODE.SUCCESS;
            response.message = MESSAGES.UPDATE_SUCCESSFULLY;
            response.data = event;
    
            callback(null, response);
        } catch (err) {
            error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
            error.message = MESSAGES.ACTION_NOT_COMPLETE;
            error.data = err;
            callback(error, null);
        }
};

module.exports.updateEventService = updateEventService;
