const { getJobService } = require('./getJob.js');
const { createJobService } = require('./createJob');
const { updateJobService } = require('./updateJob');
const { deleteJobService } = require('./deleteJob');

let handle_request = (msg, callback) => {
    switch (msg.task) {
        case 'getJobInfo':
            getJobService(msg, callback);
            break;
        case 'createJob':
            createJobService(msg, callback);
            break;
        case 'updateJobInfo':
            updateJobService(msg, callback);
            break;
        case 'deleteJob':
            deleteJobService(msg, callback);
            break;
        default:
            console.error('UNRECOGNIZED TASK', msg.task);
    }
};

module.exports = handle_request;
