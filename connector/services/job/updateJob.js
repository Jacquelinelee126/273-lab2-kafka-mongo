const Job = require('../../models/job');
const mongoose = require('mongoose');
const { STATUS_CODE, MESSAGES } = require('../../utils');

updateJobService = async (msg, callback) => {
    let response = {};
    let error = {};
    const userID = msg.userID;
    const jobID = msg.jobID;

    let updates = [];
    let msgKeys = Object.keys(msg);
    console.log('msgKeys=====>', msgKeys);

    msgKeys.forEach((key) => {
        if (key !== 'task' && key !== 'userID' && key !== 'jobID') {
            updates.push(key);
        }
    });

    console.log('updates=====>', updates);
    const allowedUpdates = [
        'title',
        'location',
        'salary',
        'jobDescription',
        'applicationDeadline',
        'category',
    ];

    const isValidOperation = updates.every((update) => allowedUpdates.includes(update));

    console.log(' isValidOperation===> ', isValidOperation);
    if (!isValidOperation) {
        error.status = STATUS_CODE.BAD_REQUEST;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = { err: 'Invalid updates!' };
        return callback(error, null);
    }

    try {
        let job = await Job.findById(jobID);
        updates.forEach((update) => (job[update] = msg[update]));
        await job.save();

        response.status = STATUS_CODE.SUCCESS;
        response.message = MESSAGES.UPDATE_SUCCESSFULLY;
        response.data = job;

        callback(null, response);
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.updateJobService = updateJobService;
