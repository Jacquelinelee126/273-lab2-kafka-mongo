const { STATUS_CODE, MESSAGES } = require('../../utils');
const Job = require('../../models/job');

getJobService = async (msg, callback) => {
    // const userID = msg.userID;
    const jobID = msg.jobID;

    let response = {};
    let error = {};
    try {
        const job = await Job.findById(jobID);

        response.status = STATUS_CODE.SUCCESS;
        response.message = MESSAGES.SUCCESS;
        response.data = job;

        callback(null, response);
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.getJobService = getJobService;
