// const Company = require('../../models/companyProfile');
const Job = require('../../models/job');
const { STATUS_CODE, MESSAGES } = require('../../utils');
createJobService = async (msg, callback) => {
    const userID = msg.userID; // userID is just a string, need to convert it into objectID

    let response = {};
    let error = {};
    console.log('MESSAGE RECEIVED ====> ', msg);
    try {
        // let user = await User.findOne({
        //     owner: mongoose.Types.ObjectId(userID),
        // });

        // // company.job.push(...msg);

        // console.log('COMPANY====>', company.job);
        // const ele = {
        //     owner: userID,
        //     title: msg.title,
        //     location: msg.location,
        //     jobDescription: msg.jobDescription,
        //     applicationDeadline: msg.applicationDeadline,
        //     category: msg.category,
        //     salary: msg.salary,
        // };

        // company.job.push(ele);
        // const result = await company.save();

        const job = new Job({
            owner: userID,
            ...msg,
        });

        await job.save();
        response.status = STATUS_CODE.SUCCESS;
        response.message = MESSAGES.SUCCESS;
        response.data = job;

        callback(null, response);
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;
        callback(error, null);
    }
};

module.exports.createJobService = createJobService;
