const { STATUS_CODE, MESSAGES } = require('../../utils');
const Job = require('../../models/job');
const mongoose = require('mongoose');

deleteJobService = async (msg, callback) => {
    const jobID = msg.jobID;

    console.log('Message Received=======>', msg);

    let response = {};
    let error = {};

    try {
        await Job.findByIdAndDelete(jobID);

        response.status = STATUS_CODE.SUCCESS;
        response.message = MESSAGES.DELETE_SUCCESSFULLY;

        callback(null, response);
    } catch (err) {
        error.status = STATUS_CODE.INTERNER_SERVER_ERROR;
        error.message = MESSAGES.ACTION_NOT_COMPLETE;
        error.data = err;

        callback(error, null);
    }
};

module.exports.deleteJobService = deleteJobService;
