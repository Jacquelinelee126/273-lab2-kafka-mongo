require('./db');
const connection = new require('./kafkaConfig/connection');
const studentService = require('./services/student');
const userService = require('./services/user');
// const profileService = require('./services/profile');
const jobService = require('./services/job');
const eventService = require('./services/event');
const companyService = require('./services/company');
const searchService = require('./services/search');
const { authenticationService } = require('./services/user/authentication');

//Handle topic request
const handleTopicRequest = (topic_name, handleRequest) => {
  var consumer = connection.getConsumer(topic_name);
  var producer = connection.getProducer();
  console.log('handleTopicRequest - Kafka Server is running - ', topic_name);
  // console.log(consumer);
  // console.log(producer)

  consumer.on('message', function(message) {
    console.log('handleTopicRequest', topic_name, ' callback - Message received:', message);
    var data = JSON.parse(message.value);
    console.log('handleTopicRequest', data);

    handleRequest(data.data, (err, res) => {
      console.log('data.data', data.data);
      console.log('Inside handleRequest - err', err);
      console.log('Inside handleRequest - res', res);

      response(data, res, err, producer);
      return;
    });
  });
};

const response = (data, res, err, producer) => {
  var payloads = [
    {
      topic: data.replyTo,
      messages: JSON.stringify({
        correlationId: data.correlationId,
        data: res,
        err: err,
      }),
      partition: 0,
    },
  ];
  producer.send(payloads, function(err, data) {
    if (err) {
      console.log('Error when producer sending data', err);
    } else {
      console.log('Data producer sending', data);
    }
  });
  return;
};
handleTopicRequest('authentication', authenticationService);
handleTopicRequest('company', companyService);
handleTopicRequest('event', eventService);
handleTopicRequest('job', jobService);
handleTopicRequest('search', searchService);
handleTopicRequest('student', studentService);
handleTopicRequest('user', userService);
