require('dotenv').config();

const express = require('express');

const bodyParser = require('body-parser');

const router = require('./routers');

const app = express();

app.use(bodyParser.json());

app.use('/api', router);

// const PORT = process.env.PORT;

app.listen(6060, () => {
  console.log('Server is listening on port: ', 6060);
});
