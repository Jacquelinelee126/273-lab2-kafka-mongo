const kafka = require('../../kafka/client');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const passport = require('passport');
const { secretKey } = require('../constants');

// Setup work and export for the JWT passport strategy
function auth() {
  var opts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: secretKey,
  };
  passport.use(
    new JwtStrategy(opts, (jwt_payload, callback) => {
      console.log('JWT PAYLOAD FROM PASSPORT', jwt_payload);
      let msg = {};
      msg.userID = jwt_payload._id;
      // msg.task = "authentication";

      kafka.make_request('authentication', msg, (err, result) => {
        console.log(err);
        console.log(result);
        if (err) {
          console.log('error in passport', err);

          return callback(err, false);
        }
        if (result) {
          console.log('user found in passport');
          return callback(null, result);
        } else {
          console.log('no user is found in passport');
          return callback(null, false);
        }
      });
    }),
  );
}

exports.auth = auth;
exports.checkAuth = passport.authenticate('jwt', { session: false });

// // 1. req sends with token
// // 2. check the user id in the token
// // 3. to see if there is user with that id in the table
// // 4. if yes, you create another token, then compare
// // to see if both tokens are the same
// // if yes, you set req.user_id = token.id

// try {
//   const user = await User.findById({ _id: msg });

//   if (user) {
//     callback(null, user);
//   } else {
//     callback(null, false);
//   }
// } catch (error) {
//   callback(error, false);
// }
