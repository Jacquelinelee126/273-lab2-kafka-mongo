const express = require('express');
const { checkAuth } = require('../utils/passport');
const kafka = require('../../kafka/client');
let router = express.Router();

router.get('/', checkAuth, (req, res) => {
  let msg = {};
  msg.userID = req.user._id;
  msg.task = 'getBasic';

  kafka.make_request('student', msg, (err, result) => {
      if (err) {
          res.json({
              error: true,
              status: err.status,
              message: err.message,
              data: err.data,
          });
      } else {
          res.json({
              error: false,
              status: result.status,
              message: result.message,
              data: result.data,
          });

          res.end();
      }
  });
});

router.post('/', checkAuth, (req, res) => {
  const msg = { ...req.body };
  console.log("msg in rest", msg);
  msg.userID = req.user._id;
  msg.task = 'createBasic';

  kafka.make_request('student', msg, (err, result) => {
      if (err) {
          res.json({
              error: true,
              status: err.status,
              message: err.message,
              data: err.data,
          });
      } else {
          res.json({
              error: false,
              status: result.status,
              message: result.message,
              data: result.data,
          });

          res.end();
      }
  });
});

router.put('/', checkAuth, (req, res) => {
    const msg = {};
    msg.body = { ...req.body };
    msg.userID = req.user._id;
    msg.task = 'updateBasic';

    kafka.make_request('student', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.status(200).json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

module.exports = router;
