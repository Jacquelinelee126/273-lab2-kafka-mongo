const express = require('express');
const { checkAuth } = require('../utils/passport');
const kafka = require('../../kafka/client');
let router = express.Router();

router.get('/', checkAuth, (req, res) => {
  let msg = {};
  msg.userID = req.user._id;
  msg.task = 'getExperience';

  kafka.make_request('student', msg, (err, result) => {
      if (err) {
          res.json({
              error: true,
              status: err.status,
              message: err.message,
              data: err.data,
          });
      } else {
          res.json({
              error: false,
              status: result.status,
              message: result.message,
              data: result.data,
          });

          res.end();
      }
  });
});

router.get('/:expID', checkAuth, (req, res) => {
    let msg = {};
    msg.userID = req.user._id;
    msg.expID = req.params.expID;
    msg.task = 'getExperienceByID';
  
    kafka.make_request('student', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });
  
            res.end();
        }
    });
  });

router.post('/', checkAuth, (req, res) => {
  const msg = { ...req.body };
  msg.userID = req.user._id;
  msg.task = 'createExperience';

  kafka.make_request('student', msg, (err, result) => {
      if (err) {
          res.json({
              error: true,
              status: err.status,
              message: err.message,
              data: err.data,
          });
      } else {
          res.json({
              error: false,
              status: result.status,
              message: result.message,
              data: result.data,
          });

          res.end();
      }
  });
});

router.put('/:expID', checkAuth, (req, res) => {
  const msg = {};
  msg.body = { ...req.body };
  msg.userID = req.user._id;
  msg.expID = req.params.expID;
  msg.task = 'updateExperience';

  kafka.make_request('student', msg, (err, result) => {
      if (err) {
          res.json({
              error: true,
              status: err.status,
              message: err.message,
              data: err.data,
          });
      } else {
          res.json({
              error: false,
              status: result.status,
              message: result.message,
              data: result.data,
          });

          res.end();
      }
  });
});

module.exports = router;
