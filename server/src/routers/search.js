const express = require('express');
const kafka = require('../../kafka/client');
let router = express.Router();
const { checkAuth } = require('../utils/passport');

router.get('/jobs', checkAuth, (req, res) => {
    let msg = {};
    msg.task = 'searchJobs';
    msg.userID = req.user._id;
    msg.keyword = req.query.keyword;
    (msg.jobCategory = req.query.category), 
    (msg.jobLocation = req.query.location);

    kafka.make_request('student', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

router.get('/events', checkAuth, (req, res) => {
    let msg = {};
    msg.page = req.query.page;
    msg.limit = req.query.limit;
    msg.title = req.query.keyword;
    msg.task = 'searchEvents';

    kafka.make_request('student', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});


router.get('/students', checkAuth, (req, res) => {
  let msg = {};
  msg.task = 'searchStudents';
  msg.major = req.query.major;
  msg.keyword = req.query.keyword;

  kafka.make_request('student', msg, (err, result) => {
      if (err) {
          res.json({
              error: true,
              status: err.status,
              message: err.message,
              data: err.data,
          });
      } else {
          res.json({
              error: false,
              status: result.status,
              message: result.message,
              data: result.data,
          });

          res.end();
      }
  });
});

module.exports = router;
