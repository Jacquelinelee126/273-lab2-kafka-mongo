const express = require('express');
const router = express.Router();
const { auth } = require('../utils/passport');
auth();

const user = require('./user');
const student = require('./student');
const company = require('./company');
const event = require('./event');
const search = require('./search');
const job = require('./job');
const basic = require('./basic');
const education = require('./education');
const experience = require('./experience');

router.use('/user', user);
router.use('/student', student);
router.use('/company', company);
router.use('/job', job);
router.use('/event', event);
router.use('/search', search);
router.use('/basic', basic);
router.use('/education', education);
router.use('/experience', experience);

router.use((req, res, next) => {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

router.use((err, req, res, next) => {
    if (err.name === 'UnauthorizedError') {
        res.status(401).json({
            code: 401,
            message: 'invalid token',
            data: err,
        });
    } else {
        res.status(err.status || 500).json({
            code: err.status || 500,
            message: err.message,
            data: err,
            request: { path: req.path, body: req.body, params: req.params, method: req.method },
        });
    }
});

module.exports = router;
