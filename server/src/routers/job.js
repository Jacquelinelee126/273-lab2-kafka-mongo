const express = require('express');
const { checkAuth } = require('../utils/passport');
const kafka = require('../../kafka/client');
let router = express.Router();

router.get('/:jID', checkAuth, async (req, res) => {
    let msg = {};
    msg.userID = req.user._id;
    msg.jobID = req.params.jID;
    msg.task = 'getJobInfo';

    kafka.make_request('job', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

router.post('/', checkAuth, (req, res) => {
    const msg = { ...req.body };
    msg.userID = req.user._id;
    msg.task = 'createJob';

    kafka.make_request('job', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});
router.put('/:jID', checkAuth, (req, res) => {
    const msg = { ...req.body };
    msg.jobID = req.params.jID;
    msg.userID = req.user._id;
    msg.task = 'updateJobInfo';

    kafka.make_request('job', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

router.delete('/:jID', checkAuth, async (req, res) => {
    let msg = {};
    msg.userID = req.user._id;
    msg.jobID = req.params.jID;
    msg.task = 'deleteJob';

    kafka.make_request('job', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

module.exports = router;
