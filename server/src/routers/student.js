const express = require('express');
const { checkAuth } = require('../utils/passport');
const kafka = require('../../kafka/client');
let router = express.Router();
const sharp = require('sharp');
const multer = require('multer');

router.get('/', checkAuth, (req, res) => {
    let msg = {};
    msg.page = req.query.page;
    msg.limit = req.query.limit;
    msg.task = 'getAllStudents';

    kafka.make_request('student', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

router.get('/profile/:sID', checkAuth, (req, res) => {
    let msg = {};
    msg.sID = req.params.sID;
    msg.task = 'getOneStudentProfile';

    kafka.make_request('student', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

router.get('/company/:company', checkAuth, (req, res) => {
    let msg = {};
    msg.company = req.params.company;
    msg.task = 'getOneCompany';

    kafka.make_request('student', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

router.get('/jobDetails/:jID', checkAuth, (req, res) => {
    let msg = {};
    msg.jID = req.params.jID;
    msg.task = 'getOneJob';

    kafka.make_request('student', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});
router.post('/', checkAuth, (req, res) => {
    const msg = { ...req.body };
    msg.userID = req.user._id;
    msg.task = 'createStudentProfile';

    kafka.make_request('student', msg, (err, result) => {
        if (err) {
            console.log('Error received from make_request: ', err);
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

router.put('/', checkAuth, (req, res) => {
    const msg = {};
    msg.body = { ...req.body };
    msg.userID = req.user._id;
    msg.task = 'updateStudentProfile';

    kafka.make_request('student', msg, (err, result) => {
        if (err) {
            console.log('Error received from make_request: ', err);
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

router.delete('/', checkAuth, (req, res) => {
    let msg = {};
    msg.userID = req.user._id;
    msg.task = 'deleteStudentProfile';

    kafka.make_request('student', msg, (err, result) => {
        if (err) {
            console.log('Error received from make_request: ', err);
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

const upload = multer({
    limits: {
        fileSize: 1000000,
    },
    fileFilter(req, file, cb) {
        if (!file.originalname.match(/\.(jpg|jpeg|png|pdf)$/)) {
            return cb(new Error('Please upload an image'));
        }

        cb(undefined, true);
    },
});

router.post('/resume', checkAuth, upload.single('resume'), async (req, res) => {
    let msg = {};
    msg.userID = req.user._id;
    msg.task = 'uploadResume';

    console.log('REQ.FILE=====>', req.file);
    msg.resume = req.file.buffer;

    kafka.make_request('user', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

// api/student/job/jid/apply
router.post('/job/:jID/apply', checkAuth, upload.single('resume'), async (req, res) => {
    let msg = {};
    msg.userID = req.user._id;
    msg.jobID = req.params.jID;
    msg.task = 'applyJob';

    const buffer = await sharp(req.file.buffer)
        .resize({ width: 250, height: 250 })
        .png()
        .toBuffer();

    msg.resume = buffer;

    kafka.make_request('student', msg, (err, result) => {
        if (err) {
            console.log('Error received from make_request: ', err);
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

// api/student/applications
router.get('/job/applications', checkAuth, (req, res) => {
    let msg = {};
    msg.userID = req.user._id;
    msg.page = req.query.page;
    msg.limit = req.query.limit;
    msg.task = 'getJobApplication';
    // req.setEncoding('binary');

    kafka.make_request('student', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.set('Content-Type', 'image/png');
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

router.post('/event/:eID/register', checkAuth, (req, res) => {
    let msg = {};
    msg.userID = req.user._id;
    msg.eventID = req.params.eID;
    msg.task = 'registerEvent';

    kafka.make_request('student', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

router.get('/event', checkAuth, (req, res) => {
    let msg = {};
    msg.page = req.query.page;
    msg.limit = req.query.limit;
    console.log('============>', req.query);
    msg.task = 'getAllEvents';

    kafka.make_request('student', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

router.get('/job', checkAuth, async (req, res) => {
    let msg = {};
    msg.page = req.query.page;
    msg.limit = req.query.limit;
    msg.task = 'getAllJobs';

    kafka.make_request('student', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

module.exports = router;
