const express = require('express');
const kafka = require('../../kafka/client');
let router = express.Router();
const { checkAuth } = require('../utils/passport');

router.get('/', checkAuth, (req, res) => {
    let msg = {};
    msg.userID = req.user._id;
    msg.task = 'getCompanyProfile';

    kafka.make_request('company', msg, (err, result) => {
        if (err) {
            console.log('Error received from make_request: ', err);
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

router.post('/', checkAuth, (req, res) => {
    const msg = { ...req.body };
    msg.userID = req.user._id;
    msg.task = 'createCompanyProfile';

    kafka.make_request('company', msg, (err, result) => {
        if (err) {
            console.log('Error received from make_request: ', err);
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

router.put('/', checkAuth, (req, res) => {
    const msg = {};
    msg.body = req.body;
    msg.userID = req.user._id;
    msg.task = 'updateCompanyProfile';

    kafka.make_request('company', msg, (err, result) => {
        if (err) {
            console.log('Error received from make_request: ', err);
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.status(200).json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

router.delete('/', checkAuth, (req, res) => {
    let msg = {};
    msg.userID = req.user._id;
    msg.task = 'deleteCompanyProfile';

    kafka.make_request('company', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

router.get('/job', checkAuth, (req, res) => {
    let msg = {};
    msg.userID = req.user._id;
    msg.task = 'getAllJobs';

    kafka.make_request('company', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

router.get('/job/:jID/application', checkAuth, (req, res) => {
    let msg = {};
    msg.userID = req.user._id;
    msg.jobID = req.params.jID;
    msg.task = 'getAllApplicationsForOneJob';

    kafka.make_request('company', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

// change status
router.put('/application/:aID', checkAuth, (req, res) => {
    // company get the application,
    // review student profile(link), resume
    let msg = {};
    msg.status = req.body.status;
    msg.applicationID = req.params.aID;
    msg.task = 'updateJobApplicationStatus';

    kafka.make_request('company', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

router.get('/job/:jid/application/:aid', checkAuth, (req, res) => {
    let msg = {};
    msg.userID = req.user._id;
    msg.jobID = req.params.jID;
    msg.studentID = req.params.aID;
    msg.task = 'getOneApplication';

    kafka.make_request('company', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

// get all events
router.get('/event', checkAuth, (req, res) => {
    let msg = {};
    msg.userID = req.user._id;
    msg.task = 'getAllEvents';

    kafka.make_request('company', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });
  
            res.end();
        }
    });
});

// get all registration from that event
router.get('/event/:eID/registration', checkAuth, (req, res) => {
    let msg = {};
    msg.userID = req.user._id;
    msg.eventID = req.params.eID;
    msg.task = 'getAllRegistrationForOneEvent';

    kafka.make_request('company', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

module.exports = router;
