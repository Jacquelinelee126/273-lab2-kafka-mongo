const express = require('express');
const { checkAuth } = require('../utils/passport');
const kafka = require('../../kafka/client');
let router = express.Router();

router.get('/:eID', checkAuth, (req, res) => {
    let msg = {};
    msg.userID = req.user._id;
    msg.eventID = req.params.eID;
    msg.task = 'getEventInfo';

    kafka.make_request('event', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

router.post('/', checkAuth, (req, res) => {
    const msg = { ...req.body };
    msg.userID = req.user._id;
    msg.task = 'createEvent';

    kafka.make_request('event', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});
router.put('/:eID', checkAuth, (req, res) => {
    const msg = { ...req.body };
    msg.userID = req.user._id;
    msg.eventID = req.params.eID;
    msg.task = 'updateEvent';

    kafka.make_request('event', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

router.delete('/:eID', checkAuth, (req, res) => {
    let msg = {};
    msg.userID = req.user._id;
    msg.eventID = req.params.eID;
    msg.task = 'deleteEvent';

    kafka.make_request('event', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

module.exports = router;
