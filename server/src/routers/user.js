const express = require('express');
const kafka = require('../../kafka/client');
const sharp = require('sharp');
const multer = require('multer');
const { checkAuth } = require('../utils/passport');
let router = express.Router();

router.post('/register', async (req, res) => {
    let msg = { ...req.body };
    msg.task = 'registration';

    kafka.make_request('user', msg, (err, result) => {
        console.log("err====>")
        if (err) {
            res.status(err.status).json({
                error: true,
                message: err.message,
                data: err.data,
            });
        } else {
            res.status(result.status).json({
                error: false,
                message: result.message,
                data: result.data,
            });

            res.end();
        }
    });
});

router.post('/login', async (req, res) => {
    let msg = { ...req.body };
    msg.task = 'login';

    kafka.make_request('user', msg, (err, result) => {
        if (err) {
            res.json({
                error: true,
                status: err.status,
                message: err.message,
                data: err.data,
            });
        } else {
            res.json({
                error: false,
                status: result.status,
                message: result.message,
                token: result.data,
            });

            res.end();
        }
    });
});

router.get('/', checkAuth, (req, res) => {
    const { id, username, email, role } = req.user;

    res.json({
        error: false,
        data: { id, username, email, role },
    });
});

const upload = multer({
    limits: {
        fileSize: 1000000,
    },
    fileFilter(req, file, cb) {
        if (!file.originalname.match(/\.(jpg|jpeg|png|pdf)$/)) {
            return cb(new Error('Please upload an image'));
        }

        cb(undefined, true);
    },
});

router.post('/avatar', checkAuth, upload.single('avatar'), async (req, res) => {
    let msg = {};
    msg.userID = req.user._id;
    msg.task = 'uploadAvatar';

    console.log('REQ.FILE=====>', req.file);
    const buffer = await sharp(req.file.buffer)
        .resize({ width: 250, height: 250 })
        .png()
        .toBuffer();
    msg.avatar = buffer;

    kafka.make_request('user', msg, (err, result) => {
        if (err) {
            res.status(400).json({
                error: true,
                data: err.message,
            });
        } else {
            res.status(200).json({
                error: false,
                data: result,
            });
            res.end();
        }
    });
});

router.post('/resume', checkAuth, upload.single('resume'), async (req, res) => {
    let msg = {};
    msg.userID = req.user._id;
    msg.task = 'uploadResume';

    console.log('REQ.FILE=====>', req.file);
    msg.resume = req.file.buffer;

    kafka.make_request('user', msg, (err, result) => {
        if (err) {
            res.status(400).json({
                error: true,
                data: err.message,
            });
        } else {
            res.status(200).json({
                error: false,
                data: result,
            });
            res.end();
        }
    });
});

// router.delete('/users/me/avatar', checkAuth, async (req, res) => {
//     req.user.avatar = undefined;
//     await req.user.save();
//     res.send();
// });

router.get('/avatar', checkAuth, async (req, res) => {
    let msg = {};
    msg.userID = req.user._id;
    msg.task = 'getAvatar';

    kafka.make_request('user', msg, (err, result) => {
        if (err) {
            res.status(400).json({
                error: true,
                data: err.message,
            });
        } else {
            res.set('Content-Type', 'image/png');

            res.status(200).send({ result });

            res.end();
        }
    });
});

// router
//     .route('/profile')
//     .get(checkAuth, async (req, res) => {
//         let msg = {};
//         msg.userID = req.user._id;
//         msg.role = req.user.role;
//         msg.task = 'getProfile';

//         kafka.make_request('profile', msg, (err, result) => {
//             if (err) {
//                 console.log('Error received from make_request: ', err);
//                 res.json({
//                     error: true,
//                     status: err.status,
//                     message: err.message,
//                     data: err.data
//                 });
//             } else {
//                 res.status(200).json({
//                     error: false,
//                     data: res.data
//                 });

//                 res.end();
//             }
//         });
//     })
//     .post(checkAuth, (req, res) => {
//         const msg = { ...req.body };
//         msg.role = req.user.role;
//         msg.userID = req.user._id;
//         msg.task = 'createProfile';

//         kafka.make_request('profile', msg, (err, result) => {
//             if (err) {
//                 console.log('Error received from make_request: ', err);
//                 res.status(400).json({
//                     error: true,
//                     status: err.status,
//                     data: err.data
//                 });
//             } else {
//                 res.status(201).json({
//                     error: false,
//                     data: result,
//                 });

//                 res.end();
//             }
//         });
//     })
//     .put(checkAuth, async (req, res) => {
//         const msg = { ...req.body };
//         msg.userID = req.user._id;
//         msg.role = req.user.role;
//         msg.task = 'updateProfile';

//         kafka.make_request('profile', msg, (err, result) => {
//             if (err) {
//                 console.log('Error received from make_request: ', err);
//                 res.status(400).json({
//                     error: true,
//                     data: err.errmsg,
//                 });
//             } else {
//                 res.status(200).json({
//                     error: false,
//                     data: result,
//                 });

//                 res.end();
//             }
//         });
//     })
//     .delete(checkAuth, async (req, res) => {
//         let msg = {};
//         msg.userID = req.params.id;
//         msg.role = req.user.role;
//         msg.task = 'deleteProfile';

//         kafka.make_request('profile', msg, (err, result) => {
//             if (err) {
//                 console.log('Error received from make_request: ', err);
//                 res.status(400).json({
//                     error: true,
//                     data: err.errmsg,
//                 });
//             } else {
//                 res.status(200).json({
//                     error: false,
//                     data: result,
//                 });

//                 res.end();
//             }
//         });
//     });

module.exports = router;
