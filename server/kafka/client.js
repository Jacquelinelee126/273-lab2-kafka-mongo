var rpc = new (require("./kafkarpc"))();

//make request to kafka
function make_request(queue_name, msg_payload, callback) {
  console.log("make_request - payload:", msg_payload);
  rpc.makeRequest(queue_name, msg_payload, function(err, response) {
      console.log("make_request - err:", err, " response:", response);
      callback(err, response);
    }
  );
}

exports.make_request = make_request;

// index.js --> post --> make_request ---> rpc.makeRequest() -->
// post('/book', (req, res))
// make_request(queue_name, msg_payload, callback)
// ===> make_request('post_book', req.body, (error, result))
// rpc.makeRequest(topic_name, content, callback)
// ===> makeRequest('post_book', req.body, (err, response))
