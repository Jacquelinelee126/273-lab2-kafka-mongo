const crypto = require("crypto");
const conn = require("./connection");

let TIMEOUT = 8000; //time to wait for response in ms
var self;

function KafkaRPC() {
  self = this;
  this.connection = conn;
  this.requests = {}; //hash to store request in wait for response
  this.response_queue = false; //placeholder for the future queue
  this.producer = this.connection.getProducer();
}

// rpc.makeRequest(queue_name, msg_payload, function(err, response)
KafkaRPC.prototype.makeRequest = function(topic_name, content, callback) {
  self = this;
  //generate a unique correlation id for this call
  var correlationId = crypto.randomBytes(16).toString("hex");

  //create a timeout for what should happen if we don't get a response
  var tId = setTimeout(
    function(corr_id) {
      //if this ever gets called we didn't get a response in a
      //timely fashion
      console.log("KafkaRPC.makeRequest - timeout...");
      callback(new Error("timeout " + corr_id));
      //delete the entry from hash
      delete self.requests[corr_id];
    },
    TIMEOUT,
    correlationId
  );

  //create a request entry to store in a hash
  var entry = {
    callback: callback,
    timeout: tId //the id for the timeout so we can clear it
  };

  //put the entry in the hash so we can match the response later
  self.requests[correlationId] = entry;

  //make sure we have a response topic
  self.setupResponseQueue(self.producer, topic_name, function() {
    console.log("KafkaRPC.makeRequest - in setupResponseQueue callback");
    //put the request on a topic

    var payloads = [
      {
        topic: topic_name,
        messages: JSON.stringify({
          correlationId: correlationId,
          replyTo: "response_topic",
          data: content
        }),
        partition: 0
      }
    ];

    console.log(
      "KafkaRPC.makeRequest - self.produecer.ready?",
      self.producer.ready
    );
    console.log("KafkaRPC.makeRequest - payloads:", payloads);
    self.producer.send(payloads, function(err, data) {
      console.log(
        "KafkaRPC.makeRequest - producer.send return data:",
        data,
        " err:",
        err
      );
    });
  });
};

KafkaRPC.prototype.setupResponseQueue = function(producer, topic_name, next) {
  console.log("KafkaRPC.setupResponseQueue - topic_name: ", topic_name);
  //don't mess around if we have a queue
  if (this.response_queue) return next();

  console.log(
    "KafkaRPC.setupResponseQueue - Need to initialize response queue"
  );

  self = this;

  //subscribe to messages
  const consumer = self.connection.getConsumer("response_topic");
  consumer.on("message", function(message) {
    console.log(
      "KafkaRPC.setupResponseQueue message callback - msg received from kafka",
      message
    );
    // message format when an error happens
    // {
    //   topic: 'response_topic',
    //   value: 'ValidationError: title: Path `title` is required.',
    //   offset: 138,
    //   partition: 0,
    //   highWaterOffset: 144,
    //   key: null,
    //   timestamp: 2020-04-02T04:51:54.809Z
    // }

    // message format when a request succeeds
    //   topic: 'response_topic',
    //   value: '{"correlationId":"6886db0444cd7c34009eb12f3e061ab3","data":{"_id":"5e856b2d7ff1a1fa2e20b677","title":"book666","createdAt":"2020-04-02T04:33:49.382Z","updatedAt":"2020-04-02T04:33:49.382Z","__v":0}}',
    //   offset: 137,
    //   partition: 0,
    //   highWaterOffset: 138,
    //   key: null,
    //   timestamp: 2020-04-02T04:33:49.387Z
    // }
    var data = JSON.parse(message.value);

    console.log(
      "KafkaRPC.setupResponseQueue message callback - message.value: ",
      data
    );
    //get the correlationId
    var correlationId = data.correlationId;
    //is it a response to a pending request
    if (correlationId in self.requests) {
      //retrieve the request entry
      var entry = self.requests[correlationId];
      //make sure we don't timeout by clearing it
      clearTimeout(entry.timeout);
      //delete the entry from hash
      delete self.requests[correlationId];

      if (data.err) {
        entry.callback(data.err, null);
      } else {
        entry.callback(null, data.data);
      }
      //callback, no err
    } else {
      console.error(`Unknown correlation id ${correlationId}`);
    }
  });
  self.response_queue = true;
  console.log("KafkaRPC.setupResponseQueue - Initialized");
  return next();
};

exports = module.exports = KafkaRPC;
